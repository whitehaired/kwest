﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Java.IO;

using Kwest.UI.Andro.ViewModel;

namespace Kwest.UI.Andro.ExtensionMethods
{
    public static class ContentResolverExtensions
    {
        public static byte[] GetBytes(this ContentResolver resolver, Android.Net.Uri uri)
        {
            try
            {
                using (var bufferStream = new ByteArrayOutputStream())
                using (var inputStream = resolver.OpenInputStream(uri))
                {
                    const int bufferSize = 1024;
                    for (var buffer = new byte[bufferSize]; inputStream.Read(buffer) != 0;)
                    {
                        bufferStream.Write(buffer);
                    }
                    return bufferStream.ToByteArray();
                }
            }
            catch (FileNotFoundException ex)
            {
                return null;
            }
        }

        public static FileDetails GetFileDetails(this ContentResolver resolver, Android.Net.Uri uri)
        {
            var columns = new string[] { MediaStore.MediaColumns.DisplayName, MediaStore.MediaColumns.MimeType };
            var cursor = resolver.Query(uri, columns, null, null, null);
            cursor.MoveToFirst();
            var displayNameColumnIndex = cursor.GetColumnIndex(MediaStore.MediaColumns.DisplayName);
            var mimeTypeColumnIndex = cursor.GetColumnIndex(MediaStore.MediaColumns.MimeType);

            return new FileDetails() { DisplayName = cursor.GetString(displayNameColumnIndex), MimeType = cursor.GetString(mimeTypeColumnIndex) };
        }
    }
}