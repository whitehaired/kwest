﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;

using Kwest.ViewModels.Login;

namespace Kwest.UI.Andro.ExtensionMethods
{
    public static class NavigationViewExtensionMethods
    {
        public static void DisplayAnonymousUserMenu(this NavigationView navigation) => ToggleIMenu(navigation);

        public static void DisplayLoggedInUserMenu(this NavigationView navigation, LoggedInUserViewModel user) => ToggleIMenu(navigation, user);

        private static void ToggleIMenu(NavigationView navigation, LoggedInUserViewModel user = null)
        {
            var userLoggedIn = user != null;

            navigation.Menu.FindItem(Resource.Id.menu_group_account_logged).SetVisible(userLoggedIn);
            navigation.Menu.FindItem(Resource.Id.menu_group_account_anonymous).SetVisible(!userLoggedIn);

            var header = navigation.GetHeaderView(0);
            var welcomeMessage = header.FindViewById<LinearLayout>(Resource.Id.linear_navigation_user_welcome);
            var messageUserNameDisplay = header.FindViewById<TextView>(Resource.Id.text_logged_in_user_nickname);

            if (userLoggedIn)
            {
                welcomeMessage.Visibility = ViewStates.Visible;
                messageUserNameDisplay.Text = user.Nickname;
            }
            else
            {
                messageUserNameDisplay.Text = String.Empty;
                welcomeMessage.Visibility = ViewStates.Gone;
            }
        }
    }
}