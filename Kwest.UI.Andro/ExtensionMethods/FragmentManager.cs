﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Kwest.UI.Andro.ExtensionMethods
{
    public static class FragmentManager
    {
        public static int PopBackStackNone(this Android.Support.V4.App.FragmentManager fragmentManager)
        {
            return 0;   // seems like Microsoft didn't specify a const for it in v4 :/
        }
    }
}