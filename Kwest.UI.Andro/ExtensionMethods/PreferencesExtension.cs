﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Kwest.ViewModels.Login;
using Newtonsoft.Json;

namespace Kwest.UI.Andro.ExtensionMethods
{
    public static class PreferencesExtension
    {
        private static readonly string loggedInUserKey = "LoggedInUser";
        
        public static LoggedInUserViewModel User(this ISharedPreferences preferences)
        {
            if (preferences.UserLoggedIn())
            {
                var userData = preferences.GetString(loggedInUserKey, null);
                return JsonConvert.DeserializeObject<LoggedInUserViewModel>(userData);
            }
            return null;
        }

        public static void LogOut(this ISharedPreferences preferences)
        {
            if (UserLoggedIn(preferences))
            {
                preferences.Edit().Remove(loggedInUserKey).Apply();
            }
        }

        public static bool UserLoggedIn(this ISharedPreferences preferences) => preferences.Contains(loggedInUserKey);

        public static void User(this ISharedPreferences preferences, LoggedInUserViewModel model ) =>
            preferences.Edit()
                .PutString(loggedInUserKey, JsonConvert.SerializeObject(model))
                .Apply();
    }
}