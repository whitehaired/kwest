﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Kwest.UI.Andro.ExtensionMethods
{
    public static class ValueTypeExtensions
    {
        public static Nullable<T> AsNullable<T>(this T valueType) where T : struct => new Nullable<T>(valueType);
    }
}