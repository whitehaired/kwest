﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Kwest.UI.Andro.ExtensionMethods
{
    public static class DecimalExtensions
    {
        public static decimal? AsNullable(this decimal value) => value.AsNullable<decimal>();
    }
}