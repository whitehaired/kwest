﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

using Firebase.Iid;

using Kwest.APIClient;
using Kwest.Configurations.APIRoutes.BaseAddress;
using Kwest.UI.Andro.SQLite;
using Kwest.UI.Andro.SQLite.Model;

namespace Kwest.UI.Andro.Services
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";
        private IUserClient users;

        public override void OnCreate()
        {
            base.OnCreate();
            this.users = new UserClient(Developement.Resource.AndroidEmulator);
        }

        public override void OnTokenRefresh()
        {
            using (var sqlite = new AndroDB())
            {
                var id = sqlite.UserId()?.Id;
                if (id != null)
                {
                    users.UpdateDeviceKey(id, Firebase.Iid.FirebaseInstanceId.Instance.Token);
                }
            }
        }
    }
}