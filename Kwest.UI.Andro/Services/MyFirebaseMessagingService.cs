﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Media;
using Android.Util;
using Firebase.Messaging;
using Android.Support.V4.App;

using Kwest.UI.Andro.Activities;

namespace Kwest.UI.Andro.Services
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";
        public override void OnMessageReceived(RemoteMessage message)
        {
            var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.CHANNEL_ID)
                                      .SetSmallIcon(Resource.Drawable.ic_menu_camera)
                                      .SetContentTitle("Kwest")
                                      .SetContentText(message.GetNotification().Body)
                                      .SetAutoCancel(true)
                                      .SetContentIntent(this.CreateIntent(message));

            NotificationManagerCompat.From(this).Notify(MainActivity.NOTIFICATION_ID, notificationBuilder.Build());
        }
        
        private PendingIntent CreateIntent(RemoteMessage message)
        {
            var intent = message.ToIntent();
            intent.AddFlags(ActivityFlags.ClearTop);

            return PendingIntent.GetActivity(this, MainActivity.NOTIFICATION_ID, intent, PendingIntentFlags.OneShot);
        }
    }
}