﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Kwest.UI.Andro.ViewModel
{
    public class FileDetails
    {
        public string DisplayName { get; set; }
        public string MimeType { get; set; }
    }
}