﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

using Kwest.UI.Andro.SQLite.Model;

namespace Kwest.UI.Andro.SQLite
{
    public class AndroDB : IDisposable
    {
        private string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        private string userIdTableName = "UserID.db";

        SQLiteConnection connection;

        public AndroDB()
        {
            this.connection = new SQLiteConnection(System.IO.Path.Combine(folder, userIdTableName));
            this.connection.CreateTable<UserId>();
        }

        public void UserId(UserId id)
        {
            this.connection.DeleteAll<UserId>();
            this.connection.Insert(id);
        }

        public UserId UserId() => this.connection.Table<UserId>().FirstOrDefault();

        public void Dispose() => this.connection.Close();
    }
}