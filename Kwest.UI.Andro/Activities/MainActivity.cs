﻿using System;

using Android.App;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;

using Kwest.UI.Andro.Fragments;
using Kwest.UI.Andro.ExtensionMethods;
using Android.Widget;

using Android.Gms.Common;
using Firebase.Messaging;
using Firebase.Iid;
using Android.Util;

namespace Kwest.UI.Andro.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, NavigationView.IOnNavigationItemSelectedListener
    {
        static readonly string TAG = "MainActivity";

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            this.CreateNotificationChannel();

            this.InitializeNavigationUI();
            this.DisplayHomeFragment();
        }

        void CreateNotificationChannel()
        {
            if(Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                var channel = new NotificationChannel(CHANNEL_ID, "FCM Notifications", NotificationImportance.Default)
                {

                    Description = "Firebase Cloud Messages appear in this channel"
                };
                var notificationManager = (NotificationManager)GetSystemService(NotificationService);
                notificationManager.CreateNotificationChannel(channel);

            }
        }

        private void InitializeNavigationUI()
        {
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            var drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            var toggle = new ActionBarDrawerToggle(this, drawer, toolbar, Resource.String.navigation_drawer_open, Resource.String.navigation_drawer_close);
            drawer.AddDrawerListener(toggle);
            toggle.SyncState();

            var navigationView = FindViewById<NavigationView>(Resource.Id.navigation);
            navigationView.SetNavigationItemSelectedListener(this);

            var preferences = PreferenceManager.GetDefaultSharedPreferences(this);

            if (preferences.UserLoggedIn())
            {
                navigationView.DisplayLoggedInUserMenu(preferences.User());
            }
            else
            {
                navigationView.DisplayAnonymousUserMenu();
            }
        }

        private void DisplayHomeFragment()
        {
            var home = new HomeFragment();
            SupportFragmentManager.BeginTransaction().Add(containerViewId: Resource.Id.body, fragment: home).Commit();
            SupportFragmentManager.BeginTransaction().Replace(containerViewId: Resource.Id.body, fragment: home).AddToBackStack(HomeFragment.BackStackTag).Commit();
        }

        public override void OnBackPressed()
        {
            DrawerLayout drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            if (drawer.IsDrawerOpen(GravityCompat.Start))
            {
                drawer.CloseDrawer(GravityCompat.Start);
            }
            else
            {
                base.OnBackPressed();
            }
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            var transaction = SupportFragmentManager.BeginTransaction();
            Android.Support.V4.App.Fragment fragment = null;
            string tag = null;

            var drawer = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            drawer.CloseDrawer(GravityCompat.Start);

            switch (item.ItemId)
            {
                case Resource.Id.navigation_home:
                    fragment = new HomeFragment();
                    break;
                case Resource.Id.navigation_collection:
                    fragment = new CollectionIndexFragment();
                    tag = CollectionIndexFragment.BackStackTag;
                    break;
                case Resource.Id.navigation_ranking:
                    fragment = new RankingFragment();
                    break;
                case Resource.Id.navigation_register:
                    fragment = new RegisterFragment();
                    break;
                case Resource.Id.navigation_login:
                    fragment = new LoginFragment();
                    break;
                case Resource.Id.navigation_achievements:
                    fragment = new UserAchievementsFragment();
                    break;
                case Resource.Id.navigation_logout:
                    PreferenceManager.GetDefaultSharedPreferences(this).LogOut();

                    var navigation = this.FindViewById<NavigationView>(Resource.Id.navigation);
                    navigation.DisplayAnonymousUserMenu();

                    this.SupportFragmentManager.PopBackStack(HomeFragment.BackStackTag, SupportFragmentManager.PopBackStackNone());
                    return true;
            }
            transaction.Replace(containerViewId: Resource.Id.body, fragment: fragment).AddToBackStack(tag).Commit();
            return true;
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}