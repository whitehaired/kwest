﻿using System;

using Android.App;
using Android.Runtime;

using Unity;

using Kwest.Configurations.APIRoutes.BaseAddress;
using Kwest.Configurations.IoCContainer;

namespace Kwest.UI.Andro
{
    [Application]
    public class MainApp : Application
    {
        public UnityContainer Container { get; private set; }
        public MainApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        { }
        public MainApp() : base() { }

        public override void OnCreate()
        {
            base.OnCreate();
            Container = new UnityContainer();
            DefaultContainerConfigurations.UseAuthClient(Container, baseAddress: Developement.Auth.AndroidEmulator);
            DefaultContainerConfigurations.UseCollectionClient(Container, Developement.Resource.AndroidEmulator);
            DefaultContainerConfigurations.UsePaymentClient(Container, Developement.Resource.AndroidEmulator);
            DefaultContainerConfigurations.UseGameClient(Container, Developement.Resource.AndroidEmulator);
            DefaultContainerConfigurations.UseUserClient(Container, Developement.Resource.AndroidEmulator);
        }
    }
}