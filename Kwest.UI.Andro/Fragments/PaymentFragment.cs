﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

using Unity;

using Kwest.APIClient;
using Kwest.UI.Andro.ExtensionMethods;
using Kwest.ViewModels.Payment;
using Android.Preferences;
using Kwest.ViewModels.Payment.API;

namespace Kwest.UI.Andro.Fragments
{
    public class PaymentFragment : Android.Support.V4.App.Fragment
    {
        WebView web;

        PaymentViewModel payment;
        PaymentAPIResponse response;

        IPaymentClient payments;

        public PaymentFragment(PaymentViewModel payment)
        {
            this.payment = payment;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var application = this.Activity.Application as MainApp;
            payments = application.Container.Resolve<IPaymentClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) =>
            inflater.Inflate(Resource.Layout.partial_payment, container, attachToRoot: false);

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            var response = await this.payments.Pay(this.payment);
            if (response.Succeeded)
            {
                this.response = response;
                this.DisplayPaymentSite(response.RedirectURL);
            }
        }

        private void DisplayPaymentSite(string paymentUrl)
        {
            this.web = this.View.FindViewById<WebView>(Resource.Id.web_payment_body);
            web.Settings.JavaScriptEnabled = true;
            web.SetWebViewClient(new PaymentWebClient(payment.BaseContinueURL)
            {
                OnPaymentEnded = PaymentEnded
            });
            web.LoadUrl(paymentUrl);
        }

        private void PaymentEnded()
        {
            this.payments.CheckPaymentStatus(response.LocalPaymentId);
            this.Activity?.SupportFragmentManager?.PopBackStackImmediate(CollectionSingleFragment.BackStackTag, this.FragmentManager.PopBackStackNone());
        }
    }
}