﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Unity;

using Kwest.APIClient;
using Kwest.Core.ResponseAdapters.CreateCollection;
using Kwest.ViewModels;
using Kwest.ViewModels.Collection;
using Kwest.UI.Andro.ExtensionMethods;

namespace Kwest.UI.Andro.Fragments
{
    public class CollectionCreateFragment : Android.Support.V4.App.Fragment
    {
        private const int MainImageIntent = 1;
        private const int MainVideoIntent = 2;

        private byte[] mainImage;
        private string mainImageMime;

        private byte[] mainVideo;
        private string mainVideoMime;

        private TextView mainImageFilenameLabel;
        private TextView mainVideoFilenameLabel;

        ICollectionClient collections;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var application = this.Activity.Application as MainApp;
            collections = application.Container.Resolve<ICollectionClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) =>
            inflater.Inflate(Resource.Layout.partial_collection_create, container, attachToRoot: false);

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            this.InitializeViews();
            this.AddHandlers();
        }

        private void InitializeViews()
        {
            this.mainImageFilenameLabel = this.View.FindViewById<TextView>(Resource.Id.textView_main_image_name);
            this.mainVideoFilenameLabel = this.View.FindViewById<TextView>(Resource.Id.textView_main_video_name);
        }

        private void AddHandlers()
        {
            var mainImageButton = this.View.FindViewById<Button>(Resource.Id.button_main_image);
            mainImageButton.Click += OnMainImageButtonClicked;
            
            var mainVideoButton = this.View.FindViewById<Button>(Resource.Id.button_main_video);
            mainVideoButton.Click += OnMainVideoButtonClicked;

            var createCollectionButton = this.View.FindViewById<Button>(Resource.Id.button_create_collection);
            createCollectionButton.Click += OnCreateButtonClicked;
        }

        private void OnMainImageButtonClicked(object source, EventArgs args) =>
            this.StartActivityForResult(this.CreateChooser("Main Image", "image/jpeg"), MainImageIntent);

        private void OnMainVideoButtonClicked(object source, EventArgs args) =>
            this.StartActivityForResult(this.CreateChooser("Main Video", "video/mp4"), MainVideoIntent);

        private Intent CreateChooser(string title, string type) =>
            Intent.CreateChooser(new Intent(), title)
                .SetAction(Intent.ActionGetContent)
                .SetType(type);

        private async void OnCreateButtonClicked(object sender, EventArgs args)
        {
            var response = new CreateCollectionResponseAdapter(await this.collections.Create(this.CollectFormData()));
            if (response.Ok)
            {
                this.NavigateBackToIndex();
            }
            else
            {
                this.DisplayValidationErrors(response);
            }
        }

        private CollectionFormDTO CollectFormData()
        {
            var title = this.View.FindViewById<EditText>(Resource.Id.editText_title).Text;
            var moneyNeeded = this.View.FindViewById<EditText>(Resource.Id.editText_money_needed).Text;
            var begins = this.View.FindViewById<DatePicker>(Resource.Id.datePicker_begins).DateTime;
            var ends = this.View.FindViewById<DatePicker>(Resource.Id.datePicker_ends).DateTime;
            var description = this.View.FindViewById<EditText>(Resource.Id.editText_description).Text;

            var preferences = PreferenceManager.GetDefaultSharedPreferences(this.Activity);
            return new CollectionFormDTO()
            {
                Title = title,
                MoneyNeeded = moneyNeeded.TryParseToDecimalOrNull(),
                Begins = begins,
                Ends = ends,
                MainImage = this.mainImage,
                Description = description,
                MainVideo = this.mainVideo,
                CurrentUserId = preferences.UserLoggedIn() ? preferences.User().Id : null
            };
        }

        private void NavigateBackToIndex() => 
            this.Activity.SupportFragmentManager.PopBackStack(CollectionIndexFragment.BackStackTag, FragmentManager.PopBackStackNone());

        private void DisplayValidationErrors(CreateCollectionResponseAdapter errors)
        {
            this.ChildFragmentManager.BeginTransaction()
                .Replace(Resource.Id.frame_general_errors, new ErrorGroupFragment(errors.GeneralErrors))
                .Replace(Resource.Id.frame_title, new ErrorGroupFragment(errors.Title))
                .Replace(Resource.Id.frame_money_needed, new ErrorGroupFragment(errors.MoneyNeeded))
                .Replace(Resource.Id.frame_begins, new ErrorGroupFragment(errors.Begins))
                .Replace(Resource.Id.frame_ends, new ErrorGroupFragment(errors.Ends))
                .Replace(Resource.Id.frame_main_image, new ErrorGroupFragment(errors.MainImage))
                .Replace(Resource.Id.frame_description, new ErrorGroupFragment(errors.Description))
                .Replace(Resource.Id.frame_main_video, new ErrorGroupFragment(errors.MainVideo))
                .Commit();
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            
            if (resultCode == (int)Result.Ok && data != null)
            {
                this.SetFile(requestCode, data.Data);
            }
        }

        private void SetFile(int requestCode, Android.Net.Uri fileUri)
        {
            var resolver = this.Context.ContentResolver;
            var details = resolver.GetFileDetails(fileUri);
            var file = resolver.GetBytes(fileUri);
            var mime = details.MimeType;

            switch (requestCode)
            {
                case MainImageIntent:
                    this.mainImage = file;
                    this.mainImageMime = mime;
                    this.mainImageFilenameLabel.Text = details.DisplayName;
                    break;
                case MainVideoIntent:
                    this.mainVideo = file;
                    this.mainVideoMime = mime;
                    this.mainVideoFilenameLabel.Text = details.DisplayName;
                    break;
            }
        }
    }
}