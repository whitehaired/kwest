﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Unity;

using Kwest.APIClient;

namespace Kwest.UI.Andro.Fragments
{
    public class RankingFragment : Android.Support.V4.App.Fragment
    {
        private IGameClient game;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var application = this.Activity.Application as MainApp;
            this.game = application.Container.Resolve<IGameClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ranking = inflater.Inflate(Resource.Layout.partial_users_ranking, container, attachToRoot: false) as ScrollView;
            var table = ranking.FindViewById<TableLayout>(Resource.Id.table_users_ranking);
            this.DisplayUserRanks(table);
            return ranking;
        }

        private async void DisplayUserRanks(TableLayout table)
        {
            var users = await this.game.UserRanking();
            var length = users.Count();
            for (var index = 0; index < length; ++index)
            {
                var user = users.ElementAt(index);
                var row = LayoutInflater.From(this.Context).Inflate(Resource.Layout.users_ranking_row, table, attachToRoot: false);
                row.FindViewById<TextView>(Resource.Id.text_position).Text = (index+1).ToString();
                row.FindViewById<TextView>(Resource.Id.text_nickname).Text = user.Nickname;
                row.FindViewById<TextView>(Resource.Id.text_points).Text = user.Points;
                table.AddView(row);
            }
        }
    }
}