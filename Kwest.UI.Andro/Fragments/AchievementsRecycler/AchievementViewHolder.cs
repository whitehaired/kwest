﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Kwest.ViewModels.Achievement;

namespace Kwest.UI.Andro.Fragments.AchievementsRecycler
{
    public class AchievementViewHolder : RecyclerView.ViewHolder
    {
        View item;

        public AchievementViewHolder(View item) : base(item)
        {
            this.item = item;
        }

        public void SetModel(AchievementViewModel achievement)
        {
            this.item.FindViewById<TextView>(Resource.Id.textView_achievement_title).Text = achievement.Title;
            this.item.FindViewById<TextView>(Resource.Id.textView_achievement_description).Text = achievement.Description;

            var progressBar = this.item.FindViewById<ProgressBar>(Resource.Id.progressBar_achievement_progress);
            progressBar.Max = achievement.RequiredRepetitions;
            progressBar.Progress = achievement.Progress;

            this.item.FindViewById<TextView>(Resource.Id.textView_achievement_progress_label).Text = $"{achievement.Progress}/{achievement.RequiredRepetitions}";

            var rewardDescription = this.item.FindViewById<TextView>(Resource.Id.textView_achievement_reward_description);
            rewardDescription.Text = achievement.Reward.Description;

            var reward = this.item.FindViewById<ToggleButton>(Resource.Id.button_achievement_reward);
            reward.Text = achievement.Reward.Title;
            reward.TextOn = achievement.Reward.Title;
            reward.TextOff = achievement.Reward.Title;
            reward.CheckedChange += (sender, args) =>
            {
                if (args.IsChecked)
                {
                    rewardDescription.Visibility = ViewStates.Visible;
                }
                else
                {
                    rewardDescription.Visibility = ViewStates.Gone;
                }
            };
        }
    }
}