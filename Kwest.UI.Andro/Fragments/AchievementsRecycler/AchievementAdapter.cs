﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Kwest.ViewModels.Achievement;

namespace Kwest.UI.Andro.Fragments.AchievementsRecycler
{
    public class AchievementAdapter : RecyclerView.Adapter
    {
        IEnumerable<AchievementViewModel> achievements;

        public AchievementAdapter(IEnumerable<AchievementViewModel> achievements)
        {
            this.achievements = achievements;
        }

        public override int ItemCount => achievements.Count();

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var achievementView = holder as AchievementViewHolder;
            achievementView.SetModel(achievements.ElementAt(position));
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var item = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.achievement_item, parent, attachToRoot: false);
            return new AchievementViewHolder(item);
        }
    }
}