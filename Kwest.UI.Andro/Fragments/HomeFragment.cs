﻿using System;

using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Unity;

using Kwest.APIClient;
using Kwest.UI.Andro.Fragments.CollectionIndexRecycler;
using System.Collections.Generic;

namespace Kwest.UI.Andro.Fragments
{
    public class HomeFragment : Android.Support.V4.App.Fragment
    {
        public static readonly string BackStackTag = "HOME";

        private ICollectionClient collections;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var application = this.Activity.Application as MainApp;
            this.collections = application.Container.Resolve<ICollectionClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) =>
            inflater.Inflate(Resource.Layout.partial_home, container, false);


        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            var pickedCollections = this.View.FindViewById<RecyclerView>(Resource.Id.recycler_picked_collections);
            var contentManager = new LinearLayoutManager(this.Context, LinearLayoutManager.Horizontal, reverseLayout: false);
            pickedCollections.SetLayoutManager(contentManager);

            pickedCollections.SetAdapter(new CollectionAdapter(this.Activity, await collections.Picked()));
        }
    }
}