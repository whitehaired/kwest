﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

using Unity;

using Kwest.APIClient;
using Kwest.UI.Andro.Fragments.CollectionIndexRecycler;
using Kwest.UI.Andro.ExtensionMethods;
using Android.Preferences;
using Kwest.ViewModels.Collection;
using Android.Support.Design.Widget;

using Kwest.Core;
using Kwest.ViewModels.Collection;

namespace Kwest.UI.Andro.Fragments
{
    public class CollectionIndexFragment : Android.Support.V4.App.Fragment
    {
        public const string BackStackTag = "CollectionIndex";

        ICollectionClient collections;

        Button createCollectionButton;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var application = this.Activity.Application as MainApp;
            this.collections = application.Container.Resolve<ICollectionClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) =>
            inflater.Inflate(Resource.Layout.partial_collection_index, container, attachToRoot: false);

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            this.InitializeViews();
            this.AddHandlers();
            this.ShowUIHiddenForAnonymous();
        }

        private async void InitializeViews()
        {
            this.createCollectionButton = this.View.FindViewById<Button>(Resource.Id.button_create_collection);
            
            var indexContent = this.View.FindViewById<RecyclerView>(Resource.Id.recycler_index_content);

            var contentManager = new LinearLayoutManager(this.Context, LinearLayoutManager.Horizontal, reverseLayout: false);
            indexContent.SetLayoutManager(contentManager);

            indexContent.SetAdapter(new CollectionAdapter(this.Activity, await this.collections.Get()));

            var items = new List<string>() { "Default", "Money needed(ascending)", "Money needed(descening)", "Timeleft (ascending)", "Timeleft (descending)" };
            var adapter = new ArrayAdapter<string>(this.Context, Android.Resource.Layout.SimpleSpinnerItem, items);
            var orderby = this.View.FindViewById<Spinner>(Resource.Id.spinner_order_by);
            orderby.Adapter = adapter;
        }

        private void AddHandlers()
        {
            this.createCollectionButton.Click += CreateCollectionClicked;
            var searchFormToggler = this.View.FindViewById<ToggleButton>(Resource.Id.toggle_search_form);
            searchFormToggler.CheckedChange += this.OnSearchFormClicked;
            var search = this.View.FindViewById<Button>(Resource.Id.button_search);
            search.Click += OnSearchClicked;
        }

        private void CreateCollectionClicked(object sender, EventArgs args) =>
            this.Activity.SupportFragmentManager
                .BeginTransaction()
                .Replace(Resource.Id.body, new CollectionCreateFragment())
                .AddToBackStack(null)
                .Commit();

        private void OnSearchFormClicked(object sender, CompoundButton.CheckedChangeEventArgs args)
        {
            var searchForm = this.View.FindViewById<LinearLayout>(Resource.Id.linear_search_form);
            if(args.IsChecked)
            {
                searchForm.Visibility = ViewStates.Visible;
            }
            else
            {
                searchForm.Visibility = ViewStates.Gone;
            }
        }

        private async void OnSearchClicked(object sender, EventArgs args)
        {
            var result = await this.collections.Get(this.CollectSearchFormData());
            var notFoundMessage = this.View.FindViewById<TextView>(Resource.Id.text_not_found);
            var indexContent = this.View.FindViewById<RecyclerView>(Resource.Id.recycler_index_content);

            if (result == null)
            {
                notFoundMessage.Visibility = ViewStates.Visible;
                indexContent.Visibility = ViewStates.Gone;
            }
            else
            {
                notFoundMessage.Visibility = ViewStates.Gone;
                indexContent.SetAdapter(new CollectionAdapter(this.Activity, result));
                indexContent.Visibility = ViewStates.Visible;
            }
        }

        private CollectionFindViewModel CollectSearchFormData() =>
            new CollectionFindViewModel()
            {
                OnlyFinished = this.View.FindViewById<ToggleButton>(Resource.Id.toggle_only_finished).Checked,
                OrderCategory = (OrderCategory)this.View.FindViewById<Spinner>(Resource.Id.spinner_order_by).SelectedItemId,
                SearchPhrase = this.View.FindViewById<TextInputEditText>(Resource.Id.inputEdit_search_phrase).Text
            };

        private void ShowUIHiddenForAnonymous()
        {
            var preferences = PreferenceManager.GetDefaultSharedPreferences(this.Activity);
            if(preferences.UserLoggedIn())
            {
                this.createCollectionButton.Visibility = ViewStates.Visible;
            }
        }
    }
}