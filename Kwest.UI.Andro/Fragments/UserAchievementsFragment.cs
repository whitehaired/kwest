﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

using Unity;

using Kwest.APIClient;
using Kwest.UI.Andro.ExtensionMethods;
using Kwest.UI.Andro.Fragments.AchievementsRecycler;

namespace Kwest.UI.Andro.Fragments
{
    public class UserAchievementsFragment : Android.Support.V4.App.Fragment
    {
        IGameClient game;

        RecyclerView root;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var application = this.Activity.Application as MainApp;
            this.game = application.Container.Resolve<IGameClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            this.root = inflater.Inflate(Resource.Layout.partial_user_achievements, container, attachToRoot: false) as RecyclerView;
            this.root.SetLayoutManager(new LinearLayoutManager(this.Context, LinearLayoutManager.Vertical, reverseLayout: false));
            //DisplayAchievements();
            return root;
        }

        public async override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            var userId = PreferenceManager.GetDefaultSharedPreferences(this.Activity).User().Id;
            this.root.SetAdapter(new AchievementAdapter(await game.UserAchievements(userId)));
        }

        //private async void DisplayAchievements()
        //{
        //    var achievements = await game.UserAchievements(PreferenceManager.GetDefaultSharedPreferences(this.Activity).User().Id);
        //    root.AddView(this.CreateAchievementItemView(achievements.ElementAt(1)));
        //    root.AddView(this.CreateAchievementItemView(achievements.ElementAt(0)));
        //}

        //private View CreateAchievementItemView(AchievementViewModel achievement)
        //{
        //    var achievementView = LayoutInflater.From(root.Context).Inflate(Resource.Layout.achievement_item, root, attachToRoot: false);

        //    achievementView.FindViewById<TextView>(Resource.Id.textView_achievement_title).Text = achievement.Title;
        //    achievementView.FindViewById<TextView>(Resource.Id.textView_achievement_description).Text = achievement.Description;

        //    var progressBar = achievementView.FindViewById<ProgressBar>(Resource.Id.progressBar_achievement_progress);
        //    progressBar.Max = achievement.RequiredRepetitions;
        //    progressBar.Progress = achievement.Progress;

        //    achievementView.FindViewById<TextView>(Resource.Id.textView_achievement_progress_label).Text = $"{achievement.Progress}/{achievement.RequiredRepetitions}";

        //    var rewardDescription = achievementView.FindViewById<TextView>(Resource.Id.textView_achievement_reward_description);
        //    rewardDescription.Text = achievement.Reward.Description;

        //    var reward = achievementView.FindViewById<ToggleButton>(Resource.Id.button_achievement_reward);
        //    reward.Text = achievement.Reward.Title;
        //    reward.TextOn = achievement.Reward.Title;
        //    reward.TextOff = achievement.Reward.Title;
        //    reward.CheckedChange += (sender, args) =>
        //    {
        //        if (args.IsChecked)
        //        {
        //            rewardDescription.Visibility = ViewStates.Visible;
        //        }
        //        else
        //        {
        //            rewardDescription.Visibility = ViewStates.Gone;
        //        }
        //    };

        //    return achievementView;
        //}
    }
}