﻿using System.Collections.Generic;

using Android.OS;
using Android.Views;
using Android.Widget;

namespace Kwest.UI.Andro.Fragments
{
    public class ErrorGroupFragment : Android.Support.V4.App.Fragment
    {
        private IEnumerable<string> errors;
        private LinearLayout errorContainer;

        public ErrorGroupFragment(IEnumerable<string> errors)
        {
            this.errors = errors;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            this.errorContainer = inflater.Inflate(Resource.Layout.partial_error_group, container, attachToRoot: false) as LinearLayout;
            AddErrorsToContainer();
            return this.errorContainer;
        }

        private void AddErrorsToContainer()
        {
            foreach(var error in errors)
            {
                var textViewError = LayoutInflater.Inflate(Resource.Layout.partial_error_item, this.errorContainer, attachToRoot: false) as TextView;
                textViewError.Text = error;
                this.errorContainer.AddView(textViewError);
            }
        }
    }
}