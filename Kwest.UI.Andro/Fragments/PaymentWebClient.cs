﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace Kwest.UI.Andro.Fragments
{
    class PaymentWebClient : WebViewClient
    {
        public Action OnPaymentEnded { get; set; }

        private string appCallbackTag;

        public PaymentWebClient(string appCallbackTag)
        {
            this.appCallbackTag = appCallbackTag;
        }

        public override void OnLoadResource(WebView view, string url)
        {
            if (url.Contains(appCallbackTag))
            {
                view.StopLoading();
                OnPaymentEnded?.Invoke();
            }
        }
    }
}