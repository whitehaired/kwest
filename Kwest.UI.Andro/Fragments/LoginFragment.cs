﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;

using Firebase.Iid;
using Newtonsoft.Json;
using Unity;

using Kwest.APIClient;
using Kwest.UI.Andro.SQLite;
using Kwest.UI.Andro.SQLite.Model;
using Kwest.UI.Andro.ExtensionMethods;
using Kwest.ViewModels.Login;

namespace Kwest.UI.Andro.Fragments
{
    public class LoginFragment : Android.Support.V4.App.Fragment
    {
        IAuthClient client;
        IUserClient users;
        TextInputEditText textInputLogin;
        TextInputEditText textInputPassword;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var activity = this.Activity.Application as MainApp;
            this.client = activity.Container.Resolve<IAuthClient>();
            this.users = activity.Container.Resolve<IUserClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) =>
            inflater.Inflate(Resource.Layout.partial_login, container, attachToRoot: false);

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            this.InitializeViews();
            this.AddHandlers();
        }
        
        private void InitializeViews()
        {
            this.textInputLogin = View.FindViewById<TextInputEditText>(Resource.Id.textInput_login);
            this.textInputPassword = View.FindViewById<TextInputEditText>(Resource.Id.textInput_password);
        }
        
        private void AddHandlers()
        {
            var loginButton = this.View.FindViewById<Button>(Resource.Id.button_login);
            loginButton.Click += this.OnLoginButtonClicked;
        }
        
        private async void OnLoginButtonClicked(object sender, EventArgs args)
        {
            var loginAttempt = await client.Login(this.CollectFormData());
            if (loginAttempt.Succeeded)
            {
                var loggedInUser = new LoggedInUserViewModel(loginAttempt.Token, loginAttempt.Nickname, loginAttempt.Id);
                await this.users.UpdateDeviceKey(loggedInUser.Id, FirebaseInstanceId.Instance.Token);

                using (var sqlite = new AndroDB())
                {
                    sqlite.UserId(new UserId() { Id = loggedInUser.Id });
                }
                
                this.DisplayLoggedInUserMenuBar(loggedInUser);
                this.SaveLoggedInUser(loggedInUser);
                this.NavigateBackToHome();
            }
            else
            {
                this.ResetForm();
                this.DisplayErrors(loginAttempt.GeneralErrors);
            }
        }

        private LoginFormDTO CollectFormData()
        {
            var login = this.textInputLogin.Text;
            var password = this.textInputPassword.Text;

            return new LoginFormDTO() { Username = login, Password = password };
        }
        
        private void DisplayLoggedInUserMenuBar(LoggedInUserViewModel user)
        {
            var navigation = this.Activity.FindViewById<NavigationView>(Resource.Id.navigation);
            navigation.DisplayLoggedInUserMenu(user);
        }

        private void SaveLoggedInUser(LoggedInUserViewModel user) => 
            PreferenceManager.GetDefaultSharedPreferences(this.Activity)
                .Edit()
                .PutString("LoggedInUser", JsonConvert.SerializeObject(user))
                .Apply();

        private void NavigateBackToHome() =>
            this.Activity.SupportFragmentManager.PopBackStack(HomeFragment.BackStackTag, FragmentManager.PopBackStackNone());

        private void ResetForm()
        {
            this.textInputLogin.Text = String.Empty;
            this.textInputPassword.Text = String.Empty;
        }
        
        private void DisplayErrors(IEnumerable<string> errors)
        {
            ChildFragmentManager.BeginTransaction()
                .Replace(Resource.Id.frame_general_errors, new ErrorGroupFragment(errors))
                .Commit();
        }
    }
}