﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

using Kwest.ViewModels.Collection;
using Android.Graphics;

namespace Kwest.UI.Andro.Fragments.CollectionIndexRecycler
{
    public class CollectionViewHolder : RecyclerView.ViewHolder
    {
        public string CollectionID { get; private set; }
        private TextView title;
        private ImageView mainImage;
        private TextView owner;
        private TextView timeLeft;
        private ProgressBar moneyCollected;
        private TextView moneyCollectedLabel;

        private CollectionIconViewModel model;
        public CollectionIconViewModel Model { get => this.model; set => this.SetModel(value); }

        public CollectionViewHolder(View item) : base(item)
        {
            this.title = item.FindViewById<TextView>(Resource.Id.textView_title);
            this.mainImage = item.FindViewById<ImageView>(Resource.Id.imageView_main_image);
            this.owner = item.FindViewById<TextView>(Resource.Id.textView_owner);
            this.timeLeft = item.FindViewById<TextView>(Resource.Id.textView_time_left);
            this.moneyCollected = item.FindViewById<ProgressBar>(Resource.Id.progressBar_money_collected);
            this.moneyCollectedLabel = item.FindViewById<TextView>(Resource.Id.textView_money_collected_label);
        }

        private void SetModel(CollectionIconViewModel model)
        {
            this.CollectionID = model.Id;

            this.model = model;
            this.title.Text = model.Title;

            var image = model.MainImage;
            var bitmap = BitmapFactory.DecodeByteArray(image, offset: 0, image.Length);
            this.mainImage.SetImageBitmap(bitmap);

            this.owner.Text = model.Owner;
            this.timeLeft.Text = model.TimeLeft;
            this.moneyCollected.Max = Convert.ToInt32(model.MoneyNeeded);
            this.moneyCollected.Progress = Convert.ToInt32(model.MoneyCollected);
            this.moneyCollectedLabel.Text = model.MoneyCollected.ToString();
        }
    }
}