﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Android.Support.V7.Widget;

using Kwest.ViewModels.Collection;

namespace Kwest.UI.Andro.Fragments.CollectionIndexRecycler
{
    public class CollectionAdapter : RecyclerView.Adapter
    {
        private FragmentActivity parent;
        private RecyclerView container;
        private IEnumerable<CollectionIconViewModel> collections;

        public CollectionAdapter(FragmentActivity parent, IEnumerable<CollectionIconViewModel> collections)
        {
            this.parent = parent;
            this.collections = collections;
        }

        public override void OnAttachedToRecyclerView(RecyclerView recyclerView)
        {
            this.container = recyclerView;
            base.OnAttachedToRecyclerView(recyclerView);
        }

        public override int ItemCount => collections.Count();

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var collection = holder as CollectionViewHolder;
            var model = collections.ElementAt(position);
            collection.Model = model;

            collection.ItemView.Click -= OnCollectionItemClicked;
            collection.ItemView.Click += OnCollectionItemClicked;
        }

        private void OnCollectionItemClicked(object sender, EventArgs args)
        {
            var collection = this.container.FindContainingViewHolder(sender as View) as CollectionViewHolder;
            this.parent.SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.body, new CollectionSingleFragment(collection.CollectionID))
                .AddToBackStack(CollectionSingleFragment.BackStackTag)
                .Commit();
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var item = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.recycler_collection_item, parent, attachToRoot: false);
            return new CollectionViewHolder(item);
        }
    }
}