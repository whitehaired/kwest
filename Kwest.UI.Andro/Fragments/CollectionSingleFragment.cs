﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Android.Util;

using Unity;

using Kwest.APIClient;
using Kwest.UI.Andro.ExtensionMethods;
using Kwest.ViewModels.Payment;
using Kwest.ViewModels.Collection;

namespace Kwest.UI.Andro.Fragments
{
    public class CollectionSingleFragment : Android.Support.V4.App.Fragment
    {
        public const string BackStackTag = "SINGLE";

        ICollectionClient collections;
        IPaymentClient payments;

        private string collectionId;

        private CollectionViewModel model;

        private TextView title;
        private TextView owner;
        private ImageView mainImage;
        private ProgressBar moneyCollected;
        private TextView moneyCollectedLabel;
        private TextView collectionFinishedLabel;
        private LinearLayout dateGroup;
        private TextView begins;
        private TextView ends;
        private TextView daysLeft;
        private TextView description;
        private VideoView mainVideoPlayer;

        private ToggleButton donationDisplayer;
        private LinearLayout donationForm;
        private TextInputEditText donationValue;
        private Button donate;

        private LinearLayout loggedInUserTogglers;

        private ToggleButton pickFormDisplayer;
        private LinearLayout pickForm;
        private Button pick;

        private TextView pickRewardMissingMessage;
        private LinearLayout pickRewardMessages;
        private TextView pickRewardAmount;
        private TextView collectionAlreadyPicked;
        private Button stillPickedCollectionMessage;

        private ToggleButton freePaymentDisplayer;
        private LinearLayout freePaymentForm;
        private Button freePayment;

        private TextView freePaymentRewardMissing;
        private LinearLayout freePaymentMessages;
        private TextView freePaymentRewardAmount;

        public CollectionSingleFragment(string collectionId)
        {
            this.collectionId = collectionId;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var application = this.Activity.Application as MainApp;
            collections = application.Container.Resolve<ICollectionClient>();
            this.payments = application.Container.Resolve<IPaymentClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) =>
            inflater.Inflate(Resource.Layout.partial_collection_single, container, attachToRoot: false);

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            var response = await collections.Get(collectionId, PreferenceManager.GetDefaultSharedPreferences(this.Context).User()?.Id);
            if (response != null)
            {
                this.model = response;
                this.InitializeViews();
                this.DisplayModel();
                this.AddHandlers();
            }

            base.OnViewCreated(view, savedInstanceState);
        }

        private void InitializeViews()
        {
            this.title = this.View.FindViewById<TextView>(Resource.Id.textView_collection_title);
            this.owner = this.View.FindViewById<TextView>(Resource.Id.textView_collection_owner);
            this.mainImage = this.View.FindViewById<ImageView>(Resource.Id.imageView_main_image);
            this.moneyCollected = this.View.FindViewById<ProgressBar>(Resource.Id.progressBar_money_collected);
            this.moneyCollectedLabel = this.View.FindViewById<TextView>(Resource.Id.textView_money_collected_label);
            this.collectionFinishedLabel = this.View.FindViewById<TextView>(Resource.Id.textView_collection_finished);
            this.dateGroup = this.View.FindViewById<LinearLayout>(Resource.Id.linearLayout_date_group);
            this.begins = this.View.FindViewById<TextView>(Resource.Id.textView_collection_begins);
            this.ends = this.View.FindViewById<TextView>(Resource.Id.textView_collection_ends);
            this.daysLeft = this.View.FindViewById<TextView>(Resource.Id.textView_collection_days_left);
            this.description = this.View.FindViewById<TextView>(Resource.Id.textView_collection_description);
            this.mainVideoPlayer = this.View.FindViewById<VideoView>(Resource.Id.videoView_collection_main_video);

            this.donationDisplayer = this.View.FindViewById<ToggleButton>(Resource.Id.toggle_donation_displayer);
            this.donationForm = this.View.FindViewById<LinearLayout>(Resource.Id.linear_donation_form);
            this.donationValue = this.View.FindViewById<TextInputEditText>(Resource.Id.editText_donation_value);
            this.donate = this.View.FindViewById<Button>(Resource.Id.button_donate);

            this.loggedInUserTogglers = this.View.FindViewById<LinearLayout>(Resource.Id.linear_logged_in_user_togglers);

            this.pickFormDisplayer = this.View.FindViewById<ToggleButton>(Resource.Id.toggle_picking_displayer);
            this.pickForm = this.View.FindViewById<LinearLayout>(Resource.Id.linear_pick_form);
            this.pick = this.View.FindViewById<Button>(Resource.Id.button_pick);

            this.pickRewardMissingMessage = this.View.FindViewById<TextView>(Resource.Id.linear_no_pick_reward_message);
            this.pickRewardMessages = this.View.FindViewById<LinearLayout>(Resource.Id.linear_pick_reward_messages);

            this.pickRewardAmount = this.View.FindViewById<TextView>(Resource.Id.textView_pick_reward_amount);
            this.collectionAlreadyPicked = this.View.FindViewById<TextView>(Resource.Id.textView_already_picked);
            this.stillPickedCollectionMessage = this.View.FindViewById<Button>(Resource.Id.button_still_picked);

            this.freePaymentDisplayer = this.View.FindViewById<ToggleButton>(Resource.Id.toggle_free_payment_displayer);
            this.freePaymentForm = this.View.FindViewById<LinearLayout>(Resource.Id.linear_free_payment_form);
            this.freePayment = this.View.FindViewById<Button>(Resource.Id.button_free_payment);

            this.freePaymentRewardMissing = this.View.FindViewById<TextView>(Resource.Id.linear_no_free_payment_message);
            this.freePaymentMessages = this.View.FindViewById<LinearLayout>(Resource.Id.linear_free_payment_messages);
            this.freePaymentRewardAmount = this.View.FindViewById<TextView>(Resource.Id.textView_free_payment_reward_amount);
    }

        private void DisplayModel()
        {
            this.title.Text = this.model.Title;
            this.owner.Text = this.model.Owner;
            this.DisplayMainImage();
            this.DisplayProgress();
            if (model.Finished)
            {
                this.donationDisplayer.Visibility = ViewStates.Gone;
                this.collectionFinishedLabel.Visibility = ViewStates.Visible;
            }
            else
            {
                this.dateGroup.Visibility = ViewStates.Visible;
                this.begins.Text = model.Begins.Date.ToString("dd.MM.yyyy");
                this.ends.Text = model.Ends.Date.ToString("dd.MM.yyyy");
                this.daysLeft.Text = model.TimeLeft;
            }
            this.description.Text = model.Description;
            this.DisplayMainVideo();

            var preferences = PreferenceManager.GetDefaultSharedPreferences(this.Context);

            if (preferences.UserLoggedIn() && preferences.User().Id != model.OwnerId)
            {
                this.loggedInUserTogglers.Visibility = ViewStates.Visible;

                if (model.Rewards.HasPickReward)
                {
                    this.pickRewardMessages.Visibility = ViewStates.Visible;
                    this.pickRewardAmount.Text = $"Currently you have {this.model.Rewards.PickRewardAmount} rewards that allow you to pick collection.";

                    if (model.Rewards.HasPickedCollection)
                    {
                        if (model.Rewards.PickedCollectionId == this.collectionId)
                        {
                            this.collectionAlreadyPicked.Visibility = ViewStates.Visible;
                        }
                        else
                        {
                            this.stillPickedCollectionMessage.Visibility = ViewStates.Visible;
                            this.stillPickedCollectionMessage.Click += (sender, args) =>
                            {
                                this.Activity.SupportFragmentManager.BeginTransaction()
                                    .Replace(Resource.Id.body, new CollectionSingleFragment(this.model.Rewards.PickedCollectionId))
                                    .AddToBackStack(null)
                                    .Commit();
                            };
                        }
                    }
                }
                else
                {
                    this.pickRewardMissingMessage.Visibility = ViewStates.Visible;
                }

                if (model.Rewards.HasFreePaymentReward)
                {
                    this.freePaymentMessages.Visibility = ViewStates.Visible;
                    this.freePaymentRewardAmount.Text = $"Currently you have {model.Rewards.FreePaymentAmount} rewards that allow you to financialy support a collection.";
                }
                else
                {
                    this.freePaymentRewardMissing.Visibility = ViewStates.Visible;
                }
            }
        }

        private void DisplayMainImage() =>
            this.mainImage.SetImageBitmap(Android.Graphics.BitmapFactory.DecodeByteArray(this.model.MainImage, offset: 0, this.model.MainImage.Length));

        private void DisplayProgress()
        {
            this.moneyCollected.Max = Convert.ToInt32(this.model.MoneyNeeded);
            this.moneyCollected.Progress = Convert.ToInt32(this.model.MoneyCollected);
            this.moneyCollectedLabel.Text = this.model.MoneyCollected.ToString();
        }

        private void DisplayMainVideo()
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var mainVideoFilePath = Path.Combine(path, "tempMainVideo.mp4");

            File.WriteAllBytes(mainVideoFilePath, this.model.MainVideo);
            var media = new MediaController(this.Context);
            media.SetAnchorView(this.mainVideoPlayer);

            this.mainVideoPlayer.SetVideoPath(mainVideoFilePath);
            this.mainVideoPlayer.SetMediaController(media);
            var size = new DisplayMetrics();
            this.Activity.WindowManager.DefaultDisplay.GetMetrics(size);

            var width = size.WidthPixels;
            this.mainVideoPlayer.LayoutParameters.Width = width;
            this.mainVideoPlayer.LayoutParameters.Height = width;
        }

        private void AddHandlers()
        {
            this.donationDisplayer.CheckedChange += OnDonationDisplayerClicked;
            this.donate.Click += OnDonateClicked;

            this.pickFormDisplayer.CheckedChange += OnPickDisplayerClicked;
            this.pick.Click += OnPickClicked;

            this.freePaymentDisplayer.CheckedChange += OnFreePaymentDisplayerClicked;
            this.freePayment.Click += OnFreePaymentClicked;
        }

        private void OnDonationDisplayerClicked(object sender, CompoundButton.CheckedChangeEventArgs args)
        {
            if (args.IsChecked)
            {
                this.donationForm.Visibility = ViewStates.Visible;
            }
            else
            {
                this.donationForm.Visibility = ViewStates.Gone;
            }
        }

        private void OnDonateClicked(object sender, EventArgs args)
        {
            var donationValue = this.donationValue.Text.TryParseToDecimalOrNull();
            if (donationValue.HasValue && donationValue.Value >= 2)
            {
                this.Activity.SupportFragmentManager.BeginTransaction()
                    .Replace(Resource.Id.body, new PaymentFragment(this.CreatePayment(donationValue.Value)))
                    .AddToBackStack(null)
                    .Commit();
            }
            else
            {
                Toast.MakeText(this.Context, "Value has to be 2 or more", ToastLength.Long).Show();
            }
        }

        private PaymentViewModel CreatePayment(decimal donationValue) =>
            new PaymentViewModel()
            {
                Amount = donationValue,
                CollectionID = this.model.Id,
                CollectionTitle = this.model.Title,
                UserID = PreferenceManager.GetDefaultSharedPreferences(this.Context).User()?.Id,
                BaseContinueURL = "http://android//kwest//app//callback"
            };

        private void OnPickDisplayerClicked(object sender, CompoundButton.CheckedChangeEventArgs args)
        {
            if (args.IsChecked)
            {
                this.pickForm.Visibility = ViewStates.Visible;
            }
            else
            {
                this.pickForm.Visibility = ViewStates.Gone;
            }
        }

        private async void OnPickClicked(object sender, EventArgs args)
        {
            var result = await this.collections.Pick(this.collectionId, PreferenceManager.GetDefaultSharedPreferences(this.Context).User()?.Id);
            if (result is null)
            {
                this.RefreshScreen();
                Toast.MakeText(this.Context, "Collection has been picked. Thank you!", ToastLength.Long).Show();
            }
        }

        private void RefreshScreen()
        {
            var fragmentManager = this.Activity.SupportFragmentManager;

            fragmentManager.PopBackStack();
            fragmentManager.BeginTransaction()
                .Replace(Resource.Id.body, new CollectionSingleFragment(this.model.Id))
                .AddToBackStack(CollectionSingleFragment.BackStackTag)
                .Commit();
        }

        private void OnFreePaymentDisplayerClicked(object sender, CompoundButton.CheckedChangeEventArgs args)
        {
            if (args.IsChecked)
            {
                this.freePaymentForm.Visibility = ViewStates.Visible;
            }
            else
            {
                this.freePaymentForm.Visibility = ViewStates.Gone;
            }
        }

        private async void OnFreePaymentClicked(object sender, EventArgs args)
        {
            await this.payments.FreePay(new PaymentViewModel()
            {
                CollectionID = this.collectionId,
                UserID = PreferenceManager.GetDefaultSharedPreferences(this.Activity).User().Id
            });
            this.RefreshScreen();
        }
    }
}