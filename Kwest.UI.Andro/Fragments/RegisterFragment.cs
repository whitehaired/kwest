﻿using System;
using System.Collections.Generic;

using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.Design.Widget;

using Unity;

using Firebase.Iid;

using Kwest.APIClient;
using Kwest.Core.ResponseAdapters.Register;
using Kwest.ViewModels.Register;
using Kwest.UI.Andro.ExtensionMethods;

namespace Kwest.UI.Andro.Fragments
{
    public class RegisterFragment : Android.Support.V4.App.Fragment
    {
        IAuthClient client;
        TextInputEditText inputPasswod;
        TextInputEditText inputRepassword;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var activity = this.Activity.Application as MainApp;
            this.client = activity.Container.Resolve<IAuthClient>();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) =>
            inflater.Inflate(resource: Resource.Layout.partial_register, root: container, attachToRoot: false);

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            this.InitializeViews();
            this.AddHandlers();
        }

        private void InitializeViews()
        {
            inputPasswod = this.View.FindViewById(Resource.Id.textInput_password) as TextInputEditText;
            inputRepassword = this.View.FindViewById(Resource.Id.textInput_repassword) as TextInputEditText;
        }

        private void AddHandlers()
        {
            var registerButton = this.View.FindViewById<Button>(Resource.Id.button_register);
            registerButton.Click += async (source, eventArgs) =>
            {
                var response = new RegisterResponseAdapter(await client.Register(model: this.CollectFormData()));
                if (response == null)
                {
                    this.Activity.SupportFragmentManager.PopBackStack(HomeFragment.BackStackTag, FragmentManager.PopBackStackNone());
                }
                else
                {
                    this.ResetForm();
                    this.DisplayErrors(response);
                }
            };
        }

        private RegisterFormDTO CollectFormData()
        {
            var username = this.View.FindViewById<TextInputEditText>(Resource.Id.textInput_login).Text;
            var password = this.inputPasswod.Text;
            var repassword = this.inputRepassword.Text;
            var email = this.View.FindViewById<TextInputEditText>(Resource.Id.textInput_email).Text;
            var nickname = this.View.FindViewById<TextInputEditText>(Resource.Id.textInput_nickname).Text;

            return new RegisterFormDTO()
            {
                UserName = username,
                Password = password,
                Repassword = repassword,
                Email = email,
                Nickname = nickname
            };
        }

        private void ResetForm()
        {
            this.inputPasswod.Text = String.Empty;
            this.inputRepassword.Text = String.Empty;
        }

        private void DisplayErrors(RegisterResponseAdapter response)
        {
            this.ChildFragmentManager.BeginTransaction()
                .Replace(Resource.Id.frame_general_errors, new ErrorGroupFragment(response.GeneralErrors))
                .Replace(Resource.Id.frame_username_errors, new ErrorGroupFragment(response.UserName))
                .Replace(Resource.Id.frame_password_errors, new ErrorGroupFragment(response.Password))
                .Replace(Resource.Id.frame_repassword_errors, new ErrorGroupFragment(response.Repassword))
                .Replace(Resource.Id.frame_email_errors, new ErrorGroupFragment(response.Email))
                .Replace(Resource.Id.frame_nickname_errors, new ErrorGroupFragment(response.Nickname))
                .Commit();
        }
    }
}