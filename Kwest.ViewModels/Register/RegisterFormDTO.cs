﻿using System.ComponentModel.DataAnnotations;

using Kwest.Validation.Attribute;

namespace Kwest.ViewModels.Register
{
    public class RegisterFormDTO
    {
        #region username-attributes
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        [MinLength(6, ErrorMessage = "Username cannot be shorter than 6 signs.")]
        [RequiredSyntax(RequireDigit = true, RequireUppercase = true, RequireLowercase = true,
            ErrorMessage = "Username should have at least one uppercase, lowercase characters and a digit.")]
        #endregion
        public string UserName { get; set; }

        #region password-attributes
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Password should be at least 6 characters long.")]
        [RequiredSyntax(RequireDigit = true, RequireSpecialCharacter = true, RequireUppercase = true, RequireLowercase = true,
            ErrorMessage = "Password should contain at least one uppercase and lowercase letter, a digit and a special character.")]
        [Required(ErrorMessage = "Required.")]
        #endregion
        public string Password { get; set; }

        #region repassword-attributes
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Should be same as password.")]
        #endregion
        public string Repassword { get; set; }

        #region email-attributes
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Provided email address is invalid.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        #endregion
        public string Email { get; set; }

        #region nickname-attributes
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required.")]
        [MinLength(3, ErrorMessage = "Nickname cannot be shorter than 3 signs.")]
        [RequiredSyntax(RequireUppercase = true, RequireLowercase = true,
            ErrorMessage = "Nickname should have at least one uppercase and lowercase character.")]
        #endregion
        public string Nickname { get; set; }
    }
}
