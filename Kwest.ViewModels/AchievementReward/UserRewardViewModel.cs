﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.AchievementReward
{
    public class UserRewardViewModel
    {
        public bool HasPickedCollection { get; set; }
        public string PickedCollectionId { get; set; }
        public string PickedCollectionTitle { get; set; }
        public bool HasPickReward { get; set; }
        public int PickRewardAmount { get; set; }
        public bool HasFreePaymentReward { get; set; }
        public int FreePaymentAmount { get; set; }
    }
}
