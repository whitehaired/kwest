﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.Achievement
{
    public class RewardViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
