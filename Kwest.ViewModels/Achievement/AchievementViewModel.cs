﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;

namespace Kwest.ViewModels.Achievement
{
    public class AchievementViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int RequiredRepetitions { get; set; }
        public int Progress { get; set; }
        public string Type { get; set; }
        public AchievementStatusEnum Status { get; set; }
        public RewardViewModel Reward { get; set; }
    }
}
