﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;

namespace Kwest.ViewModels.Collection
{
    public class CollectionFindViewModel
    {
        public OrderCategory OrderCategory { get; set; }
        public string SearchPhrase { get; set; }
        public bool OnlyFinished { get; set; }
    }
}
