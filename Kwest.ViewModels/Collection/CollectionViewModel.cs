﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kwest.ViewModels.AchievementReward;

namespace Kwest.ViewModels.Collection
{
    public class CollectionViewModel
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public decimal MoneyNeeded { get; set; }

        public decimal MoneyCollected { get; set; }

        public DateTime Begins { get; set; }

        public DateTime Ends { get; set; }
        
        public bool Finished { get; set; }

        public string TimeLeft { get; set; }

        public byte[] MainImage { get; set; }

        public string Description { get; set; }

        public byte[] MainVideo { get; set; }

        public string OwnerId { get; set; }
        public string Owner { get; set; }

        public UserRewardViewModel Rewards { get; set; }
    }
}
