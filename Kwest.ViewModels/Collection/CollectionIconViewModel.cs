﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.Collection
{
    public class CollectionIconViewModel
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public decimal MoneyNeeded { get; set; }

        public decimal MoneyCollected { get; set; }

        public bool Finished { get; set; }

        public string TimeLeft { get; set; }

        public byte[] MainImage { get; set; }

        public string Owner { get; set; }

    }
}
