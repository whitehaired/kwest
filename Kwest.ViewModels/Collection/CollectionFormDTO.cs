﻿using Kwest.Validation.Attribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.Collection
{
    public class CollectionFormDTO
    {
        #region title-attributes
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        #endregion
        public string Title { get; set; }

        #region money-needed-attributes
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Currency)]
        [NumberGreaterThan(0, ErrorMessage = "Money target cannot be 0 or lower")]
        #endregion
        public decimal? MoneyNeeded { get; set; }

        #region begin-date-attributes
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Date)]
        [NowOrFuture(ErrorMessage = "Begin date should be today or in the future")]
        #endregion
        public DateTime? Begins { get; set; }

        #region finish-date-attributes
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Date)]
        [DateGreaterThanProperty(propertyName: nameof(Begins), ErrorMessage = "Collection finish date should be later than begin date")]
        #endregion
        public DateTime? Ends { get; set; }

        #region mainimage-attributes
        [Required(ErrorMessage = "Required")]
        #endregion
        public byte[] MainImage { get; set; }

        #region main-image-mime-attributes
        [StringEquals(new[] { "image/jpeg" }, ErrorMessage = "Wrong file format, only jpeg allowed")]
        #endregion
        public string MainImageMime { get; set; }

        #region description-attributes
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.MultilineText)]
        #endregion
        public string Description { get; set; }

        #region mainvideo-attributes
        [Required(ErrorMessage = "Required")]
        #endregion
        public byte[] MainVideo { get; set; }

        #region main-image-mime-attributes
        [StringEquals(new[] { "video/mp4" }, ErrorMessage = "Wrong file format, only mp4 allowed")]
        #endregion
        public string MainVideoMime { get; set; }

        #region user-id-attributes
        [Required(AllowEmptyStrings = false, ErrorMessage = "You must be logged in to create a collection")]
        #endregion
        public string CurrentUserId { get; set; }
    }
}
