﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment
{
    public class PaymentOrderViewModel
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string APIID { get; set; }
        public string UserID { get; set; }
        public string CollectionID { get; set; }
    }
}
