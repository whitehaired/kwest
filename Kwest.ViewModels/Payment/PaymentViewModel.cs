﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.Payment
{
    public class PaymentViewModel
    {
        public string CollectionID { get; set; }
        public string CollectionTitle { get; set; }
        public string BaseContinueURL { get; set; }
        public bool UserLoggedIn => !String.IsNullOrEmpty(UserID);
        public string UserID { get; set; }
        public string APIID { get; set; }
        public string PaymentOrderId { get; set; }
        public decimal Amount { get; set; }
    }
}
