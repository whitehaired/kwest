﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API
{
    public class Delivery
    {
        [JsonProperty("street")]
        public string Stree { get; set; }
        [JsonProperty("postalBox")]
        public string PostalBox { get; set; }
        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State{ get; set; }
        [JsonProperty("countryCode")]
        public string CountryCode{ get; set; }
        [JsonProperty("name")]
        public string Name{ get; set; }
        [JsonProperty("recipientName")]
        public string RecipientName { get; set; }
        [JsonProperty("recipientEmail")]
        public string RecipientEmail{ get; set; }
        [JsonProperty("recipientPhone")]
        public string RecipientPhone { get; set; }
    }
}
