﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API.Order
{
    public class PaymentSettings
    {
        [JsonProperty("invoiceDisabled")]
        public string InvoiceDisable { get; set; }
    }
}
