﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API.Order
{
    public class PaymentAPIOrder
    {
        [JsonProperty("customerIp")]
        public string CustomerIP { get; set; }
        [JsonProperty("merchantPosId")]
        public string MerchantPosID { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("currencyCode")]
        public string CurrencyCode { get; set; }
        [JsonProperty("totalAmount")]
        public string TotalAmount { get; set; }
        [JsonProperty("continueUrl")]
        public string ContinueURL { get; set; }
        [JsonProperty("products")]
        public Product[] Products { get; set; }
        [JsonProperty("settings")]
        public PaymentSettings Settings { get; set; }
    }
}
