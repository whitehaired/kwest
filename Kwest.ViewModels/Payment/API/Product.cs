﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API
{
    public class Product
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("unitPrice")]
        public string UnitPrice { get; set; }
        [JsonProperty("quantity")]
        public string Quanity { get; set; }
    }
}
