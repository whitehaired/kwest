﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API
{
    public class Buyer
    {
        [JsonProperty("customerId")]
        public string Id { get; set; }
        [JsonProperty("extCustomerId")]
        public string ExternalId { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("firstName")]
        public string Name { get; set; }
        [JsonProperty("lastName")]
        public string Surname { get; set; }
        [JsonProperty("nin")]
        public string PESEL { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        //[JsonProperty("buyer.delivery")]
        //public Delivery Delivery { get; set; }
    }
}
