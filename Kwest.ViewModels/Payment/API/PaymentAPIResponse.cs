﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API
{
    public class PaymentAPIResponse
    {
        [JsonIgnore]
        public bool Succeeded => Status.Code == "SUCCESS";

        [JsonIgnore]
        public string LocalPaymentId { get; set; }

        [JsonProperty("redirectUri")]
        public string RedirectURL { get; set; }
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
        [JsonProperty("extOrderId")]
        public string ExternalOrderId { get; set; }
        [JsonProperty("status")]
        public PaymentStatus Status { get; set; }
    }
}
