﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API.Notification
{
    public class PaymentNotification
    {
        [JsonProperty("orders")]
        public PaymentNotificationOrder[] Orders { get; set; }
        [JsonProperty("status")]
        public PaymentStatus Status { get; set; }
        [JsonProperty("properties")]
        public PaymentOrderProperty[] Properties { get; set; }
    }
}
