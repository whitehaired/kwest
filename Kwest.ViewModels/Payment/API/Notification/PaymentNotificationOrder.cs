﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API.Notification
{
    public class PaymentNotificationOrder
    {
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
        //[JsonProperty("extOrderId")]
        //public string ExternalOrderId { get; set; }
        [JsonProperty("orderCreateDate")]
        public string CreateDate { get; set; }
        //[JsonProperty("notifyUrl")]
        //public string NotifyURL { get; set; }
        [JsonProperty("customerIp")]
        public string CustomerIP { get; set; }
        [JsonProperty("merchantPosId")]
        public string MerchantPOSID { get; set; }
        //[JsonProperty("validityTime")]
        //public string ValidityTime { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        //[JsonProperty("additionalDescription")]
        //public string AdditionalDescription { get; set; }
        [JsonProperty("currencyCode")]
        public string CurrencyCode { get; set; }
        [JsonProperty("totalAmount")]
        public string TotalAmount { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("buyer")]
        public Buyer Buyer { get; set; }
        [JsonProperty("products")]
        public Product[] Products { get; set; }
    }
}
