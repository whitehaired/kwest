﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kwest.ViewModels.Payment.API
{
    public class PaymentStatus
    {
        [JsonProperty("statusCode")]
        public string Code { get; set; }
        [JsonProperty("statusDesc")]
        public string Description { get; set; }
    }
}
