﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.Payment
{
    public class FreePaymentViewModel
    {
        public string CollectionID { get; set; }
        public string UserID { get; set; }
        public string RedirectURL { get; set; }
        public decimal Amount { get; set; }
    }
}
