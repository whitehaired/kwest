﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.User
{
    public class UserRankViewModel
    {
        public string Nickname { get; set; }
        public string Points { get; set; }
    }
}
