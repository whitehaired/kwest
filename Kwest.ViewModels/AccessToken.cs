﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels
{
    public class AccessToken
    {
        public AccessToken(string value, long expirationTime)
        {
            this.Value = value;
            this.ExpirationTime = expirationTime;
        }

        public string Value { get; }
        public long ExpirationTime { get; }
    }
}
