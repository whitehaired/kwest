﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.Login
{
    public class LoginResponseViewModel
    {
        public LoginResponseViewModel(IEnumerable<string> generalErrors)
        {
            this.GeneralErrors = generalErrors;
            this.Token = null;
            this.Nickname = string.Empty;
        }

        public LoginResponseViewModel(AccessToken token, string nickname, string id)
        {
            this.Token = token;
            this.Nickname = nickname;
            this.Id = id;
            this.GeneralErrors = null;
        }

        public bool Succeeded { get => GeneralErrors == null; }
        public AccessToken Token { get; private set; }
        public string Id { get; private set; }
        public string Nickname { get; private set; }

        public IEnumerable<string> GeneralErrors { get; private set; }
    }
}
