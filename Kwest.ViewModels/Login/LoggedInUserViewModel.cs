﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ViewModels.Login
{
    public class LoggedInUserViewModel
    {
        public LoggedInUserViewModel(AccessToken token, string nickname, string id)
        {
            this.Token = token;
            this.Nickname = nickname;
            this.Id = id;
        }

        public AccessToken Token { get; private set; }
        public string Nickname { get; private set; }
        public string Id { get; private set; }
    }
}
