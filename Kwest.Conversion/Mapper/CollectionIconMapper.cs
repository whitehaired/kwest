﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Collection;

namespace Kwest.Conversion.Mapper
{
    public class CollectionIconMapper : IBaseMapper<IEnumerable<Collection>>
    {
        public object Convert(IEnumerable<Collection> model) => model.Select(ToViewModel);

        protected virtual CollectionIconViewModel ToViewModel(Collection collections)
        {
            var now = DateTime.Now.Date;
            var finished = collections.Ends.Value.Date < now;
            return new CollectionIconViewModel()
            {
                Id = collections.CollectionID,
                Title = collections.Title,
                MoneyNeeded = collections.MoneyNeeded.Value,
                MoneyCollected = collections.MoneyCollected,
                Finished = finished,
                TimeLeft = finished ? "Finished" : collections.Ends.Value.Date.Subtract(now).Days.ToString(),
                MainImage = collections.MainImage,
                Owner = collections.Owner.Nickname
            };
        }
    }
}
