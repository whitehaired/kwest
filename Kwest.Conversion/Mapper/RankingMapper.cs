﻿using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Conversion.Mapper
{
    public class RankingMapper : IBaseMapper<IEnumerable<User>>
    {
        public object Convert(IEnumerable<User> model) => model.Select(ToViewModel);

        protected virtual UserRankViewModel ToViewModel(User user) =>
            new UserRankViewModel()
            {
                Nickname = user.Nickname,
                Points = user.Points.ToString()
            };
    }
}
