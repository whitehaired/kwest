﻿using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Achievement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Conversion.Mapper
{
    public class AchievementMapper : IBaseMapper<IEnumerable<AchievementProgress>>
    {
        public object Convert(IEnumerable<AchievementProgress> model) => model.Select(this.ToViewModel);

        protected virtual AchievementViewModel ToViewModel(AchievementProgress achievement) =>
            new AchievementViewModel()
            {
                Title = achievement.Achievement.Title,
                Description = achievement.Achievement.Description,
                Progress = achievement.Progress,
                RequiredRepetitions = achievement.Achievement.RequiredRepetitions,
                Type = achievement.Achievement.Type,
                Status = achievement.AchievementStatus,
                Reward = new RewardViewModel()
                {
                    Title = achievement.Achievement.Reward.Title,
                    Description = achievement.Achievement.Reward.Description
                }
            };       
    }
}
