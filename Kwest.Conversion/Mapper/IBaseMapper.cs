﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Conversion.Mapper
{
    public interface IBaseMapper<ModelType> : IBaseConverter<ModelType, object> { }
}
