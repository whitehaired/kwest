﻿using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Conversion.Mapper
{
    public class PaymentOrderMapper : IBaseMapper<PaymentOrder>
    {
        public object Convert(PaymentOrder model) =>
            new PaymentOrderViewModel()
            {
                Amount = model.Amount,
                APIID = model.APIID,
                CollectionID = model.CollectionID,
                Id = model.Id,
                UserID = model.UserID
            };
    }
}
