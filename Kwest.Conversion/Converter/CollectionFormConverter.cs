﻿using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Conversion.Converter
{
    public class CollectionFormConverter : IBaseConverter<CollectionFormDTO, Collection>
    {
        public Collection Convert(CollectionFormDTO model) => 
            new Collection()
            {
                CollectionID = Guid.NewGuid().ToString(),
                Title = model.Title,
                MoneyNeeded = model.MoneyNeeded,
                MoneyCollected = 0,
                Begins = model.Begins,
                Ends = model.Ends,
                MainImage = model.MainImage,
                Description = model.Description,
                MainVideo = model.MainVideo,
                OwnerId = model.CurrentUserId
            };
    }
}
