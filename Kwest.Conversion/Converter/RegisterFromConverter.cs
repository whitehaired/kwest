﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Register;

namespace Kwest.Conversion.Converter
{
    public class RegisterFromConverter : IBaseConverter<RegisterFormDTO, User>
    {
        public User Convert(RegisterFormDTO model) =>
            new User()
            {
                UserName = model.UserName,
                Email = model.Email,
                Nickname = model.Nickname
            };
    }
}
