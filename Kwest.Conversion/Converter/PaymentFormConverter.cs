﻿using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Conversion.Converter
{
    public class PaymentFormConverter : IBaseConverter<PaymentViewModel, PaymentOrder>
    {
        public PaymentOrder Convert(PaymentViewModel model) =>
            new PaymentOrder()
            {
                Id = model.PaymentOrderId,
                APIID = model.APIID,
                UserID = model.UserID,
                CollectionID = model.CollectionID,
                Amount = model.Amount
            };
    }
}
