﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Conversion
{
    public interface IBaseConverter<ModelType, ReturnType>
    {
        ReturnType Convert(ModelType model);
    }
}
