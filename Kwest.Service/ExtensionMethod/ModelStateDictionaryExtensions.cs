﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;

namespace Kwest.Service.ExtensionMethod
{
    public static class ModelStateDictionaryExtensions
    {
        public static void AddModelErrors(this ModelStateDictionary modelstate, string key, IEnumerable<String> errors)
        {
            errors.ToList().ForEach(error => modelstate.AddModelError(key, error));
        }

        public static IDictionary<string, IEnumerable<string>> GetErrorDictionary(this ModelStateDictionary modelstate)
        {
            return modelstate.ToDictionary(pair => pair.Key, pair => pair.Value.Errors.Select(error => error.ErrorMessage));
        }
    }
}
