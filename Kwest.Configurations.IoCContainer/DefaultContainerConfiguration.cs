﻿using System.Net.Http;
using Unity;
using Unity.Injection;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

using Kwest.DataAccess;
using Kwest.DataAccess.Data;
using Kwest.DataAccess.Data.Model;
using Kwest.APIClient;

namespace Kwest.Configurations.IoCContainer
{
    public static class DefaultContainerConfigurations
    {
        private static HttpClient http;

        static DefaultContainerConfigurations()
        {
            http = new HttpClient() ;
            http.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public static void UseAuthRepository(UnityContainer container)
        {
            container.RegisterType<DBContext>();
            container.RegisterType<IUserStore<User>, UserStore<User>>(new InjectionConstructor(container.Resolve<DBContext>()));
            container.RegisterType<UserManager<User>>(new InjectionProperty[]
            {
                new InjectionProperty(name: "PasswordValidator", value: new PasswordValidator()
                {
                    RequireDigit = true,
                    RequireLowercase = true,
                    RequireNonLetterOrDigit = true,
                    RequireUppercase = true,
                    RequiredLength = 10
                }),
                new InjectionProperty(name: "UserValidator", value: new UserValidator<User>(container.Resolve<UserManager<User>>())
                {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                })
            });
            container.RegisterType<IAuthRepository, AuthRepostiory>();
        }

        public static void UseClientRepository(UnityContainer container)
        {
            container.RegisterType<DBContext>();
            container.RegisterType<IUserRepository, DataAccess.UserRepository>();
        }
        public static void UseCollectionRepository(UnityContainer container)
        {
            container.RegisterType<DBContext>();
            container.RegisterType<ICollectionRepository, CollectionRepository>();
        }

        public static void UsePaymentRepostiory(UnityContainer container)
        {
            container.RegisterType<DBContext>();
            container.RegisterType<IPaymentRepository, PaymentRepository>();
        }

        public static void UseAuthClient(UnityContainer container, string baseAddress)
        {
            container.RegisterType<IAuthClient, AuthClient>(new InjectionConstructor(new object[] { http, baseAddress }));
        }

        public static void UseCollectionClient(UnityContainer container, string baseAddress)
        {
            container.RegisterType<ICollectionClient, CollectionClient>(new InjectionConstructor(new object[] { http, baseAddress }));
        }

        public static void UsePaymentClient(UnityContainer container, string baseAddress)
        {
            container.RegisterType<IPaymentClient, PaymentClient>(new InjectionConstructor(new object[] { baseAddress }));
        }

        public static void UseGameClient(UnityContainer container, string baseAddress)
        {
            container.RegisterType<IGameClient, GameClient>(new InjectionConstructor(new object[] { baseAddress }));
        }

        public static void UseUserClient(UnityContainer container, string baseAddress)
        {
            container.RegisterType<IUserClient, UserClient>(new InjectionConstructor(new object[] { baseAddress }));
        }
    }
}
