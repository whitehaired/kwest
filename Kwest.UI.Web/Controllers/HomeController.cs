﻿using System.Web.Mvc;
using System.Threading.Tasks;

using Kwest.APIClient;

namespace Kwest.UI.Web.Controllers
{
    public class HomeController : Controller
    {
        ICollectionClient collections;

        public HomeController(ICollectionClient collections)
        {
            this.collections = collections;
        }

        public async Task<ActionResult> Index()
        {
            return View(await collections.Picked());
        }
    }
}