﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

using Kwest.APIClient;
using Kwest.UI.Web.ExtensionMethods;
using Kwest.UI.Web.Attributes;

namespace Kwest.UI.Web.Controllers
{
    public class GameController : Controller
    {
        IGameClient game;

        public GameController(IGameClient game)
        {
            this.game = game;
        }

        [HttpGet]
        [AuthGuard(Authorized = true, Redirect = true, RedirectUrl = "~/Account/Login")]
        public async Task<ActionResult> Achievement() => View(await game.UserAchievements(Request.Cookies.User().Id));

        [HttpGet]
        public async Task<ActionResult> Ranking() => View(await game.UserRanking());
    }
}