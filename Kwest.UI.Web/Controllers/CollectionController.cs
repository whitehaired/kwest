﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using Kwest.UI.Web.ExtensionMethods;
using Kwest.APIClient;
using Kwest.ViewModels;
using Kwest.ViewModels.Collection;
using Kwest.ViewModels.Payment;
using Kwest.UI.Web.ViewModel;
using Kwest.UI.Web.Attributes;

namespace Kwest.UI.Web.Controllers
{
    public class CollectionController : Controller
    {
        ICollectionClient collections;
        IPaymentClient payment;

        public CollectionController(ICollectionClient collections, IPaymentClient payment)
        {
            this.collections = collections;
            this.payment = payment;
        }

        #region index
        [AcceptVerbs("Get", "Post")]
        public async Task<ActionResult> Index(CollectionIndexViewModel model)
        {
            if (model.Criteria != null)
            {
                return View(new CollectionIndexViewModel()
                {
                    Collections = await this.collections.Get(model.Criteria),
                    Criteria = model.Criteria
                });
            }
            return View(new CollectionIndexViewModel() { Collections = await this.collections.Get() });
        }

        #endregion

        #region single
        [HttpGet]
        public async Task<ActionResult> Single(string collectionId, string orderId = null)
        {
            if (!String.IsNullOrEmpty(orderId))
            {
                await payment.CheckPaymentStatus(orderId);
                ViewBag.Message = "Done";
            }
            var collection = (await collections.Get(collectionId, this.Request.Cookies.User()?.Id));
            return View(collection);
        }

        [HttpPost]
        public async Task<ActionResult> Single(string collectionId, string collectionTitle, decimal donationValue)
        {
            var result = await payment.Pay(this.CreatePayment(collectionId, collectionTitle, donationValue, this.Request.Url.AbsoluteUri));
            if (result.Succeeded)
            {
                return Redirect(result.RedirectURL);
            }
            else
            {
                this.ModelState.AddModelError(String.Empty, "Couldn't connect with payment service. Try again later");
            }
            var collection = await collections.Get(collectionId, this.Request.Cookies.User()?.Id);
            return View(collection);
        }

        private PaymentViewModel CreatePayment(string collectionId, string collectionTitle, decimal donationValue, string baseContinueURL)
        {
            return new PaymentViewModel()
            {
                Amount = donationValue,
                CollectionID = collectionId,
                CollectionTitle = collectionTitle,
                BaseContinueURL = baseContinueURL,
                UserID = this.Request.Cookies.UserLoggedIn() ? this.Request.Cookies.User().Id : null
            };
        }
        #endregion

        #region create
        [HttpGet]
        [AuthGuard(Authorized = true, Redirect = true, RedirectUrl = "~/Account/Login")]
        public ActionResult Create() => View();

        [HttpPost]
        [AuthGuard(Authorized = true, Redirect = true, RedirectUrl = "~/Account/Login")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TemporaryCollectionFormDTO model)
        {
            this.ModelState.Clear();
            var response = await collections.Create(this.CreateCollectionViewModel(model));
            if (response is null)
            {
                return RedirectToAction("Index", "Home");
            }
            this.ModelState.AddModelErrors(response);
            return View(model);
        }

        private CollectionFormDTO CreateCollectionViewModel(TemporaryCollectionFormDTO model) =>
            new CollectionFormDTO()
            {
                Title = model.Title,
                MoneyNeeded = model.MoneyNeeded,
                Begins = model.Begins,
                Ends = model.Ends,
                MainImage = model.MainImage?.ToBinaryArray(),
                MainImageMime = model.MainImage?.ContentType,
                Description = model.Description,
                MainVideo = model.MainVideo?.ToBinaryArray(),
                MainVideoMime = model.MainVideo?.ContentType,
                CurrentUserId = this.Request.Cookies.UserLoggedIn() ? this.Request.Cookies.User().Id : null
            };
        #endregion

        #region pick
        [HttpPost]
        [AuthGuard(Authorized = true, Redirect = true, RedirectUrl = "~/Account/Login")]
        public async Task<ActionResult> Pick(string collectionId)
        {
            var response = await this.collections.Pick(collectionId, this.Request.Cookies.User().Id);
            if(response is null)
            {
                ViewBag.Message = "Collection has been picked succesfully";
            }
            else
            {
                // do it on feature/messaging branch
            }
            return RedirectToAction("Single", new { collectionId });
        }
        #endregion

        #region free-payment
        [HttpPost]
        [AuthGuard(Authorized = true, Redirect = true, RedirectUrl = "~/Account/Login")]
        public async Task<ActionResult> FreePayment(string collectionId)
        {
            await this.payment.FreePay(new PaymentViewModel()
            {
                CollectionID = collectionId,
                UserID = this.Request.Cookies.User().Id
            });
            ViewBag.Message = "Free payment used";
            return RedirectToAction("Single", new { collectionId });
        }
        #endregion
    }
}