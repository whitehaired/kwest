﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using Kwest.APIClient;
using Kwest.UI.Web.ExtensionMethods;
using Kwest.ViewModels.Login;
using Kwest.ViewModels.Register;
using Kwest.UI.Web.Attributes;
using Kwest.Core.ResponseAdapters.Register;

namespace Kwest.UI.Web.Controllers
{
    public class AccountController : Controller
    {
        IAuthClient client;

        public AccountController(IAuthClient client)
        {
            this.client = client;
        }

        #region login
        [HttpGet]
        [AuthGuard(Authorized = false, Redirect = true)]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AuthGuard(Authorized = false, Redirect = true)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginFormDTO model)
        {
            var login = await client.Login(model);
            if (login.Succeeded)
            {
                this.Response.Cookies.User(new LoggedInUserViewModel(login.Token, login.Nickname, login.Id));

                return RedirectToAction("Index", "Home");
            }
            this.ModelState.AddModelErrors(string.Empty, login.GeneralErrors);
            return View();
        }
        #endregion

        #region logout
        [AuthGuard(Authorized = true, Redirect = true)]
        public ActionResult Logout(string url)
        {
            Response.Cookies.Add(this.CreateExpiredCookie());
            return Redirect(url);
        }

        private HttpCookie CreateExpiredCookie()
        {
            return new HttpCookie("LoggedInUser") { Expires = DateTime.Now.AddDays(-1), HttpOnly = true };
        }
        #endregion

        #region register
        [HttpGet]
        [AuthGuard(Authorized = false, Redirect = true)]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AuthGuard(Authorized = false, Redirect = true)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterFormDTO model)
        {
            ModelState.Clear();
            var response = new RegisterResponseAdapter(await client.Register(model));
            if (response.Ok)
            {
                return RedirectToAction("Index", "Home");
            }
            this.ModelState.AddModelErrors(response.Errors);

            return View(model);
        }
        #endregion
    }
}