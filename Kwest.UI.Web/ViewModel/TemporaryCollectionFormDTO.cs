﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kwest.UI.Web.ViewModel
{
    public class TemporaryCollectionFormDTO
    {
        public string Title { get; set; }

        public decimal? MoneyNeeded { get; set; }

        public DateTime? Begins { get; set; }

        public DateTime? Ends { get; set; }

        public HttpPostedFileBase MainImage { get; set; }

        public string Description { get; set; }

        public HttpPostedFileBase MainVideo { get; set; }
    }
}