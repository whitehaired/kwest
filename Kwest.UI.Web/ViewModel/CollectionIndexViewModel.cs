﻿using Kwest.ViewModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kwest.UI.Web.ViewModel
{
    public class CollectionIndexViewModel
    {
        public IEnumerable<CollectionIconViewModel> Collections { get; set; }
        public CollectionFindViewModel Criteria { get; set; }
    }
}