﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Kwest.ViewModels.Collection;

namespace Kwest.UI.Web.ExtensionMethods
{
    public static class HtmlHelper
    {
        public static MvcHtmlString ValidationSummaryFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<string> cssClasses)
        {
            var divTag = new TagBuilder("div");

            var errorMessages = new StringBuilder();
            errorMessages.Append(divTag.ToString(TagRenderMode.StartTag));

            var modelState = htmlHelper.ViewData.ModelState;
            var model = ExpressionHelper.GetExpressionText(expression);
            if (modelState.ContainsKey(model) && modelState[model].Errors.Count > 0)
            {
                var brTag = new TagBuilder("br");
                foreach (var error in modelState[model].Errors)
                {
                    var spanTag = new TagBuilder("span") { InnerHtml = HttpUtility.HtmlEncode(error.ErrorMessage) };
                    cssClasses.ToList().ForEach(cssClass => spanTag.AddCssClass(cssClass));

                    errorMessages.Append(spanTag.ToString());
                    errorMessages.Append(brTag.ToString(TagRenderMode.SelfClosing));
                }
            }
            errorMessages.Append(divTag.ToString(TagRenderMode.EndTag));

            return MvcHtmlString.Create(errorMessages.ToString());
        }

        public static MvcHtmlString DateInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<string> cssClasses)
        {
            var baseInputTag = InitializeInput(ExpressionHelper.GetExpressionText(expression), cssClasses);
            baseInputTag.Attributes.Add("type", "date");

            var propertyValue = GetPropertyValue(htmlHelper.ViewData.Model, expression);
            if (!String.IsNullOrEmpty(propertyValue) && DateTime.TryParse(propertyValue, out DateTime value))
            {
                baseInputTag.Attributes.Add("value", value.ToString("yyyy-MM-dd"));
            }

            return MvcHtmlString.Create(baseInputTag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString NumberInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<string> cssClasses)
        {
            var baseInputTag = InitializeInput(ExpressionHelper.GetExpressionText(expression), cssClasses);
            baseInputTag.Attributes.Add("type", "number");

            var propertyValue = GetPropertyValue(htmlHelper.ViewData.Model, expression);
            if(!String.IsNullOrEmpty(propertyValue) && decimal.TryParse(propertyValue, out decimal value))
            {
                baseInputTag.Attributes.Add("value", value.ToString());
            }

            return MvcHtmlString.Create(baseInputTag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString FileInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<string> cssClasses, IEnumerable<string> mimeTypes)
        {
            var baseInputTag = InitializeInput(ExpressionHelper.GetExpressionText(expression), cssClasses);
            baseInputTag.Attributes.Add("type", "file");
            baseInputTag.Attributes.Add("accept", mimeTypes.Aggregate((firstMime, secondMime) => firstMime + " " + secondMime));

            return MvcHtmlString.Create(baseInputTag.ToString(TagRenderMode.SelfClosing));
        }

        private static TagBuilder InitializeInput(string propertyName, IEnumerable<string> cssClasses)
        {
            var inputTag = new TagBuilder("input");
            inputTag.Attributes.Add("id", propertyName);
            inputTag.Attributes.Add("name", propertyName);
            inputTag.Attributes.Add("class", cssClasses.Aggregate((firstClass, secondClass) => firstClass + " " + secondClass));

            return inputTag;
        }

        private static string GetPropertyValue<TModel, TProperty>(TModel model, Expression<Func<TModel, TProperty>> expression)
        {
            if(model != null)
            {
                var numberGetter = expression.Compile();
                return numberGetter(model).ToString();
            }
            return null;
        }
    }
}