﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kwest.UI.Web.ExtensionMethods
{
    public static class ModelState
    {
        public static void AddModelErrors(this ModelStateDictionary modelstate, string key, IEnumerable<string> errors)
        {
            errors.ToList().ForEach(error => modelstate.AddModelError(key, error));
        }

        public static void AddModelErrors(this ModelStateDictionary modelstate, IDictionary<string, IEnumerable<string>> errors)
        {
            errors.ToList().ForEach(pair => pair.Value.ToList().ForEach(error => modelstate.AddModelError(pair.Key, error)));
        }
    }
}