﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

using Kwest.ViewModels.Login;

namespace Kwest.UI.Web.ExtensionMethods
{
    public static class HttpCookieCollectionExtensions
    {
        public static LoggedInUserViewModel User(this HttpCookieCollection cookies)
        {
            if (cookies.UserLoggedIn())
            {
                var userData = cookies.Get("LoggedInUser").Value;
                return JsonConvert.DeserializeObject<LoggedInUserViewModel>(userData);
            }
            return null;
        }

        public static bool UserLoggedIn(this HttpCookieCollection cookies) =>
            cookies.AllKeys.Contains("LoggedInUser");

        public static void User(this HttpCookieCollection cookies, LoggedInUserViewModel model) =>
            cookies.Set(new HttpCookie("LoggedInUser", JsonConvert.SerializeObject(model)) { HttpOnly = true });
    }
}