﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Kwest.UI.Web.ExtensionMethods
{
    public static class HttpPostedFileBaseExtensions
    {
        public static byte[] ToBinaryArray(this HttpPostedFileBase file)
        {
            if (file == null)
            {
                throw new NullReferenceException("File is not set to instance of an object");
            }

            if (file.ContentLength > 0)
            {
                return new BinaryReader(file.InputStream).ReadBytes(file.ContentLength);
            }
            
            return null;
        }
    }
}