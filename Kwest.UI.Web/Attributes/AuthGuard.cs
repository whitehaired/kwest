﻿using Kwest.UI.Web.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kwest.UI.Web.Attributes
{
    public class AuthGuard : AuthorizeAttribute
    {
        public bool Authorized { get; set; }
        public bool Redirect { get; set; }
        public string RedirectUrl { get; set; } = "~/";

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var allowed = this.Authorized == httpContext.Request.Cookies.UserLoggedIn();
            if(!allowed && this.Redirect)
            {
                httpContext.Response.Redirect(this.RedirectUrl);
            }
            return allowed;
        }
    }
}