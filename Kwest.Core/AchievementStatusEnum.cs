﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Core
{
    /*
     * InProgress - user didn't get neither reward or achievement 
     * Achieved - user got the achievement but didn't user the reward
     * Finished - user got the achievement and used a reward (mind that repetable achievements should be never finished!)
     */
    public enum AchievementStatusEnum
    {
        InProgress = 1,
        Achieved,
        Finished
    }
}
