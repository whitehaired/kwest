﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core.ExtensionMethods;

namespace Kwest.Core.ResponseAdapters.Register
{
    public class RegisterResponseAdapter : BaseAdapter, IRegisterErrors
    {
        public RegisterResponseAdapter(IDictionary<string, IEnumerable<string>> errors) : base(errors)
        {}

        public IEnumerable<string> UserName => Errors.GetValueOrEmpty("UserName");
        public IEnumerable<string> Password => Errors.GetValueOrEmpty("Password");
        public IEnumerable<string> Repassword => Errors.GetValueOrEmpty("Repassword");
        public IEnumerable<string> Email => Errors.GetValueOrEmpty("Email");
        public IEnumerable<string> Nickname => Errors.GetValueOrEmpty("Nickname");
    }
}
