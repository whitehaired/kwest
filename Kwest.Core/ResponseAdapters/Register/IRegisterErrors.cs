﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Core.ResponseAdapters.Register
{
    public interface IRegisterErrors
    {
        IEnumerable<string> UserName { get; }
        IEnumerable<string> Password { get; }
        IEnumerable<string> Repassword { get; }
        IEnumerable<string> Email { get; }
        IEnumerable<string> Nickname { get; }
    }
}
