﻿using Kwest.Core.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Core.ResponseAdapters.CreateCollection
{
    public class CreateCollectionResponseAdapter : BaseAdapter, ICollectionValidationErrors
    {
        public CreateCollectionResponseAdapter(IDictionary<string, IEnumerable<string>> errors) : base(errors)
        { }

        public IEnumerable<string> Title => this.Errors.GetValueOrEmpty("Title");

        public IEnumerable<string> MoneyNeeded => this.Errors.GetValueOrEmpty("MoneyNeeded");

        public IEnumerable<string> Begins => this.Errors.GetValueOrEmpty("Begins");

        public IEnumerable<string> Ends => this.Errors.GetValueOrEmpty("Ends");

        public IEnumerable<string> MainImage => this.Errors.GetValueOrEmpty("MainImage");

        public IEnumerable<string> Description => this.Errors.GetValueOrEmpty("Description");

        public IEnumerable<string> MainVideo => this.Errors.GetValueOrEmpty("MainVideo");
    }
}
