﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Core.ResponseAdapters.CreateCollection
{
    public interface ICollectionValidationErrors
    {
        IEnumerable<string> Title { get; }
        IEnumerable<string> MoneyNeeded { get; }
        IEnumerable<string> Begins { get; }
        IEnumerable<string> Ends { get; }
        IEnumerable<string> MainImage { get; }
        IEnumerable<string> Description { get; }
        IEnumerable<string> MainVideo { get; }
    }
}
