﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core.ExtensionMethods;

namespace Kwest.Core.ResponseAdapters
{
    public abstract class BaseAdapter
    {
        public bool Ok => Errors == null || Errors.Count() == 0;
        public IEnumerable<string> GeneralErrors => Errors.GetValueOrEmpty(String.Empty);
        public IDictionary<string, IEnumerable<string>> Errors { get; private set; }

        public BaseAdapter(IDictionary<string, IEnumerable<string>> errors)
        {
            this.Errors = errors;
        }
    }
}
