﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Core
{
    public enum AchievementEnum
    {
        HelloKwest = 1,
        ThePatron,
        ICanHelpTooI,
        ICanHelpTooII,
        StillCheckingByI,
        StillCheckingByII
    }
}
