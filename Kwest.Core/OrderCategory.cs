﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Core
{
    public enum OrderCategory
    {
        Default,
        MoneyLeftAscending,
        MoneyLeftDescending,
        TimeLeftAscending,
        TimeLeftDescending
    }
}
