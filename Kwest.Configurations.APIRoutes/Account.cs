﻿namespace Kwest.Configurations.APIRoutes
{
    public static class Account
    {
        public const string Prefix = "account";
        public static class Actions
        {
            public const string Register = "register";
            public const string Login = "login";
        }
    }
}
