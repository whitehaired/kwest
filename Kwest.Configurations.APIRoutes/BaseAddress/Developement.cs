﻿namespace Kwest.Configurations.APIRoutes.BaseAddress
{
    public static class Developement
    {
        public static class Auth
        {
            public static readonly string AndroidEmulator = "http://10.0.2.2:55152";
            public static readonly string Web = "https://localhost:44354";
        }

        public static class Resource
        {
            public static readonly string AndroidEmulator = "http://10.0.2.2:60163";
            public static readonly string Web = "https://localhost:44390";
        }
    }
}
