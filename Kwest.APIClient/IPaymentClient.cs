﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.ViewModels.Payment.API;
using Kwest.ViewModels.Payment;

namespace Kwest.APIClient
{
    public interface IPaymentClient
    {
        Task<PaymentAPIResponse> Pay(PaymentViewModel model);
        Task CheckPaymentStatus(string orderId);
        Task FreePay(PaymentViewModel model);
    }
}
