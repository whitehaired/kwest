﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kwest.Configurations.APIRoutes;
using Kwest.ViewModels.Achievement;
using Kwest.ViewModels.User;
using Kwest.APIClient.ExtensionMethod;

namespace Kwest.APIClient
{
    public class GameClient : IGameClient
    {
        private HttpClient http;
        private string baseAddress;

        public GameClient(string baseAddress)
        {
            this.http = new HttpClient();
            this.baseAddress = baseAddress;
        }

        #region users-achievements
        public async Task<IEnumerable<AchievementViewModel>> UserAchievements(string userId)
        {
            var url = $"{baseAddress}/{Game.Prefix}/{Game.Actions.Achievement}?userId={Uri.EscapeDataString(userId)}";
            
            var response = await http.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                return await response.ReadContentAsAsync<IEnumerable<AchievementViewModel>>();
            }
            return null;
        }
        #endregion

        #region users-ranking
        public async Task<IEnumerable<UserRankViewModel>> UserRanking()
        {
            var url = $"{baseAddress}/{Game.Prefix}/ranking";

            var response = await http.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                return await response.ReadContentAsAsync<IEnumerable<UserRankViewModel>>();
            }
            return null;
        }
        #endregion
    }
}
