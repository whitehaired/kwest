﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.APIClient.ViewModel
{
    class AuthAPIResponse
    {
        public AuthAPIResponse(string access_token, string token_type, long expires_in, string error, string error_description)
        {
            this.AccessToken = access_token;
            this.TokenType = token_type;
            this.ExpiresIn = expires_in;
            this.Error = error;
            this.ErrorDescription = error_description;
        }

        [JsonProperty("access_token")]
        public string AccessToken { get; }
        [JsonProperty("token_type")]
        public string TokenType { get; }
        [JsonProperty("expires_in")]
        public long ExpiresIn { get; }
        [JsonProperty("error")]
        public string Error { get; }
        [JsonProperty("error_description")]
        public string ErrorDescription { get; }
    }
}
