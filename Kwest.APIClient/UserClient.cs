﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Kwest.APIClient
{
    public class UserClient : IUserClient
    {
        private HttpClient http;
        private string baseAddress;

        public UserClient(string baseAddress)
        {
            this.http = new HttpClient();
            this.baseAddress = baseAddress;
        }

        public async Task UpdateDeviceKey(string userId, string deviceKey) =>
            await this.http.PutAsync($"{baseAddress}/user/key?userId={userId}&deviceKey={deviceKey}", new StringContent(""));
    }
}
