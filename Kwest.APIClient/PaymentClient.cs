﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kwest.Configurations.APIRoutes;
using Kwest.ViewModels.Payment;
using Kwest.ViewModels.Payment.API;
using Kwest.ViewModels.Payment.API.Order;
using Kwest.ViewModels.Payment.API.Notification;
using Kwest.APIClient.Content;
using Kwest.APIClient.ExtensionMethod;

namespace Kwest.APIClient
{
    public class PaymentClient : IPaymentClient
    {
        private HttpClient http;
        private string baseAddress;
        private const string apiAddress = @"https://secure.snd.payu.com/";
        private const string authAddress = @"pl/standard/user/oauth/authorize";
        private const string paymentOrder = @"api/v2_1/orders";

        public PaymentClient(string baseAddress)
        {
            this.http = new HttpClient(new HttpClientHandler() { AllowAutoRedirect = false });
            this.baseAddress = baseAddress;
        }

        #region free-payment
        public async Task FreePay(PaymentViewModel model)
        {
            await this.Authorize();
            model.Amount = 10;
            var paymentProviderResponse = await this.PostToPaymentProvider(this.CreateOrder(model));
            if (paymentProviderResponse.Succeeded)
            {
                var url = $"{this.baseAddress}/{Payment.Prefix}/free";
                await this.http.PutAsync(url, new JSONContent<FreePaymentViewModel>(this.CreateFreePaymentViewModel(paymentProviderResponse, model)));
            }
        }

        private FreePaymentViewModel CreateFreePaymentViewModel(PaymentAPIResponse providerResponse, PaymentViewModel model) =>
            new FreePaymentViewModel()
            {
                CollectionID = model.CollectionID,
                UserID = model.UserID,
                RedirectURL = providerResponse.RedirectURL,
                Amount = model.Amount
            };
        #endregion

        #region pay
        public async Task<PaymentAPIResponse> Pay(PaymentViewModel model)
        {
            if (ValidateAmount(model))
            {
                model.PaymentOrderId = Guid.NewGuid().ToString();
                await this.Authorize();

                var providerResponse = await this.PostToPaymentProvider(this.CreateOrder(model));

                model.APIID = providerResponse.OrderId;
                providerResponse.LocalPaymentId = model.PaymentOrderId;

                await this.RegisterNewPaymentOrder(model);

                return providerResponse;
            }
            return new PaymentAPIResponse();
        }

        private bool ValidateAmount(PaymentViewModel model)
        {
            if (model.Amount < 1)
            {
                return false;
            }
            return true;
        }

        private async Task<PaymentAPIResponse> PostToPaymentProvider(PaymentAPIOrder order)
        {
            var url = $"{apiAddress}{paymentOrder}";

            var httpResponse = await http.PostAsync(url, new JSONContent<PaymentAPIOrder>(order));
            return await httpResponse.ReadContentAsAsync<PaymentAPIResponse>();
        }

        private PaymentAPIOrder CreateOrder(PaymentViewModel model) =>
            new PaymentAPIOrder()
            {
                CurrencyCode = "PLN",
                CustomerIP = "127.0.0.1",
                Description = $"Payment for collection {model.CollectionTitle}",
                MerchantPosID = "382549",
                Products = new Product[] {
                    new Product() { Name = $"Donation for {model.CollectionTitle}", Quanity = "1", UnitPrice = model.Amount.ToString() }
                },
                TotalAmount = (model.Amount * 100).ToString(),
                Settings = new PaymentSettings() { InvoiceDisable = "true" },
                ContinueURL = String.IsNullOrEmpty(model.BaseContinueURL) ?
                null :
                $"{model.BaseContinueURL}?collectionId={model.CollectionID}&orderId={model.PaymentOrderId}"
            };

        private async Task RegisterNewPaymentOrder(PaymentViewModel model) =>
            await http.PostAsync($"{baseAddress}/{Payment.Prefix}", new JSONContent<PaymentViewModel>(model));
        #endregion

        #region check-status
        public async Task CheckPaymentStatus(string orderId)
        {
            var orderToCheck = await this.Get(orderId);
            if (orderToCheck != null)
            {
                await this.Authorize();

                var apiUrl = $"{apiAddress}{paymentOrder}/{orderToCheck.APIID}";

                var httpResponse = await this.http.GetAsync(apiUrl);
                if (httpResponse.IsSuccessStatusCode)
                {
                    var notification = await httpResponse.ReadContentAsAsync<PaymentNotification>();
                    var url = $"{baseAddress}/{Payment.Prefix}?orderId={orderId}";
                    if (notification.Status.Code == "SUCCESS" && notification.Orders.First().Status == "COMPLETED")
                    {
                        await this.http.PutAsync(url, new StringContent(String.Empty));
                    }
                    else
                    {
                        await this.http.DeleteAsync(url);
                    }
                }
            }
        }

        public async Task<PaymentOrderViewModel> Get(string orderId)
        {
            var httpResponse = await this.http.GetAsync($"{baseAddress}/{Payment.Prefix}?orderId={orderId}");
            if (httpResponse.IsSuccessStatusCode)
            {
                return await httpResponse.ReadContentAsAsync<PaymentOrderViewModel>();
            }
            return null;
        }
        #endregion

        #region auth
        private async Task Authorize()
        {
            var url = $"{apiAddress}{authAddress}";
            var httpResponse = await this.http.PostAsync(url, this.CreateAuthContent());
            if (httpResponse.IsSuccessStatusCode)
            {
                var content = await httpResponse.ReadContentAsAsync<PaymentAPIAuthResponse>();
                this.http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", content.AccessToken);
            }
        }

        private FormUrlEncodedContent CreateAuthContent() =>
            new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("client_id", ""),
                new KeyValuePair<string, string>("client_secret", "") // I've reset the credentials but nice try :P
            });
        #endregion
    }
}
