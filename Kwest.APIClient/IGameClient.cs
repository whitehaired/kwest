﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.ViewModels.Achievement;
using Kwest.ViewModels.User;

namespace Kwest.APIClient
{
    public interface IGameClient
    {
        Task<IEnumerable<AchievementViewModel>> UserAchievements(string userId);
        Task<IEnumerable<UserRankViewModel>> UserRanking();
    }
}
