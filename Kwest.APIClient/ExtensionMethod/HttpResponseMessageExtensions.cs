﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Kwest.APIClient.ExtensionMethod
{
    public static class HttpResponseMessageExtensions
    {
        public static async Task<ReturnType> ReadContentAsAsync<ReturnType>(this HttpResponseMessage httpResponse) =>
            JsonConvert.DeserializeObject<ReturnType>(await httpResponse.Content.ReadAsStringAsync());
    }
}
