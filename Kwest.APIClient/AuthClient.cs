﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;

using Kwest.Configurations.APIRoutes;
using Kwest.ViewModels;
using Kwest.ViewModels.Register;
using Kwest.ViewModels.Login;
using Kwest.APIClient.ViewModel;
using Kwest.APIClient.Content;
using Kwest.APIClient.ExtensionMethod;

namespace Kwest.APIClient
{
    public class AuthClient : IAuthClient
    {
        private HttpClient http;
        private string baseAddress;

        public AuthClient(HttpClient http, string baseAddress)
        {
            this.http = http;
            this.baseAddress = baseAddress;
        }

        public async Task<LoginResponseViewModel> Login(LoginFormDTO model)
        {
            var content = this.CreateLoginContent(model);
            
            var uri = $"{baseAddress}/{Account.Prefix}/{Account.Actions.Login}";

            var httpResponse = await this.http.PostAsync(uri, content);
            var apiResponse = await httpResponse.ReadContentAsAsync<AuthAPIResponse>();

            if (httpResponse.IsSuccessStatusCode)
            {
                var handler = new JwtSecurityTokenHandler();
                var token = handler.ReadJwtToken(apiResponse.AccessToken);

                var jwt = new AccessToken(apiResponse.AccessToken, apiResponse.ExpiresIn);
                var nickname = token.Claims.Where(claim => claim.Type == "unique_name").Select(claim => claim.Value).FirstOrDefault();
                var id = token.Claims.Where(claim => claim.Type == "nameid").Select(claim => claim.Value).FirstOrDefault();
                return new LoginResponseViewModel(jwt, nickname, id);
            }
            return new LoginResponseViewModel(new[] { apiResponse.ErrorDescription });
        }

        private FormUrlEncodedContent CreateLoginContent(LoginFormDTO model)
        {
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("username", model.Username),
                new KeyValuePair<string, string>("password", model.Password),
                new KeyValuePair<string, string>("client_id", "ZoQYBlEXpTBmBqbYWxDYEz72T6Y5OIrY"),
                new KeyValuePair<string, string>("grant_type", "password")
            });
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");
            
            return content;
        }

        public async Task<IDictionary<string, IEnumerable<string>>> Register(RegisterFormDTO model)
        {
            var uri = $"{baseAddress}/{Account.Prefix}/{Account.Actions.Register}";
            
            var httpResponse = await this.http.PostAsync(uri, new JSONContent<RegisterFormDTO>(model));
            if(httpResponse.IsSuccessStatusCode)
            {
                return null;
            }
            var responseContent = await httpResponse.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IDictionary<string, IEnumerable<string>>>(responseContent);
        }
    }
}
