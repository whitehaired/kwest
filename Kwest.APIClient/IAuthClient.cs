﻿using System.Threading.Tasks;
using Kwest.ViewModels.Register;
using Kwest.ViewModels.Login;
using System.Collections.Generic;

namespace Kwest.APIClient
{
    public interface IAuthClient
    {
        Task<IDictionary<string, IEnumerable<string>>> Register(RegisterFormDTO model);
        Task<LoginResponseViewModel> Login(LoginFormDTO model);
    }
}
