﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.APIClient
{
    public interface IUserClient
    {
        Task UpdateDeviceKey(string userId, string deviceKey);
    }
}
