﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Kwest.APIClient.Content
{
    public class JSONContent<ContentType> : StringContent
    {
        // default encoding might be wrong - CHECK IT !!!
        public JSONContent(ContentType content) : this(content, Encoding.Default)
        { }

        public JSONContent(ContentType content, Encoding encoding) : base(JsonConvert.SerializeObject(content), encoding, "application/json")
        { }
    }
}
