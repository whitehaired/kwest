﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Kwest.Configurations.APIRoutes;
using Kwest.ViewModels.Collection;
using Kwest.ViewModels.AchievementReward;
using Kwest.Core;
using Kwest.APIClient.Content;
using Kwest.APIClient.ExtensionMethod;

namespace Kwest.APIClient
{
    public class CollectionClient : ICollectionClient
    {
        private HttpClient http;
        private string baseAddress;

        public CollectionClient(HttpClient http, string baseAddress)
        {
            this.http = http;
            this.baseAddress = baseAddress;
        }

        #region create
        public async Task<IDictionary<string, IEnumerable<string>>> Create(CollectionFormDTO model)
        {
            var uri = $"{baseAddress}/{Collection.Prefix}/";

            var httpResponse = await this.http.PostAsync(uri, new JSONContent<CollectionFormDTO>(model));
            if (httpResponse.IsSuccessStatusCode)
            {
                return null;
            }
            return await httpResponse.ReadContentAsAsync<IDictionary<string, IEnumerable<string>>>();
        }
        #endregion

        #region get
        public async Task<IEnumerable<CollectionIconViewModel>> Get() => await this.Get(new CollectionFindViewModel());
        #endregion

        #region get-id
        public async Task<CollectionViewModel> Get(string collectionId, string userId = null)
        {
            var uri = $"{baseAddress}/{Collection.Prefix}?collectionId={Uri.EscapeDataString(collectionId)}&userId={userId}";

            var httpResponse = await this.http.GetAsync(uri);
            if (httpResponse.IsSuccessStatusCode)
            {
                return await httpResponse.ReadContentAsAsync<CollectionViewModel>();
            }
            return null;
        }
        #endregion

        #region pick-collection
        public async Task<IDictionary<string, IEnumerable<string>>> Pick(string collectionId, string userId)
        {
            var httpResponse = await this.http.PutAsync($"{baseAddress}/{Collection.Prefix}?collectionId={collectionId}&userId={userId}", new StringContent(""));
            if (httpResponse.IsSuccessStatusCode)
            {
                return null;
            }
            return await httpResponse.ReadContentAsAsync<IDictionary<string, IEnumerable<string>>>();
        }
        #endregion

        #region get-picked-collections
        public async Task<IEnumerable<CollectionIconViewModel>> Picked()
        {
            var url = $"{baseAddress}/{Collection.Prefix}/picked";
            var httpResponse = await http.GetAsync(url);
            if (httpResponse.IsSuccessStatusCode)
            {
                return await httpResponse.ReadContentAsAsync<IEnumerable<CollectionIconViewModel>>();
            }
            return null; // add errror handling
        }
        #endregion

        public async Task<IEnumerable<CollectionIconViewModel>> Get(CollectionFindViewModel criteria)
        {
            var uri = $"{baseAddress}/{Collection.Prefix}?{await this.CreateFindCollectionContent(criteria)}";

            var httpResponse = await this.http.GetAsync(uri);
            if(httpResponse.IsSuccessStatusCode)
            {
                return await httpResponse.ReadContentAsAsync<IEnumerable<CollectionIconViewModel>>();
            }
            return null;
        }

        private async Task<string> CreateFindCollectionContent(CollectionFindViewModel criteria)
        {
            var content = new FormUrlEncodedContent(new Dictionary<string, string>() {
                { "SearchPhrase", criteria.SearchPhrase },
                { "OrderCategory", criteria.OrderCategory.ToString() },
                { "OnlyFinished", criteria.OnlyFinished.ToString() }
            });

            return await content.ReadAsStringAsync();
        }
    }
}
