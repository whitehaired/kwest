﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.ViewModels.Collection;
using Kwest.ViewModels.AchievementReward;

namespace Kwest.APIClient
{
    public interface ICollectionClient
    {
        Task<IDictionary<string, IEnumerable<string>>> Create(CollectionFormDTO model);

        Task<IEnumerable<CollectionIconViewModel>> Get();
        Task<CollectionViewModel> Get(string collectionId, string userId = null);
        Task<IEnumerable<CollectionIconViewModel>> Get(CollectionFindViewModel criteria);

        Task<IDictionary<string, IEnumerable<string>>> Pick(string collectionId, string userId);
        Task<IEnumerable<CollectionIconViewModel>> Picked();
    }
}
