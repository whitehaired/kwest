# Kwest - money collection app #

## Table of Contents ##
* <a href="#introduction">Introduction</a>
* <a href="#status">Status</a>
* <a href="#technologies">Technologies</a>
* <a href="#description">Description</a>
* <a href="#demo">Demo</a>
* <a href="#how-to-launch">How to launch</a>
* <a href="#screenshots">Screenshots</a>
    * <a href="#main-page">Main page</a>
    * <a href="#all-collections-view">All collections view</a>
    * <a href="#register-form">Register form</a>
    * <a href="#user-achievements">User achievement view</a>
* <a href="#features">Features</a>
    * <a href="#gamification-related-features">Gamification related features</a>

## Introduction ##

Kwest is a charity (money-collection) app that supports gamification. It makes helping others fun :)

## Status ##

In developement

## Technologies ##

* .NET 4.7
* .NET MVC 5
* Xamarin Android
* .NET Web API 2
* Entity Framework 6 (Code first)
* PayU API (sandbox version)
* Firebase Cloud Messagin (push notifications)
* Bootstrap 4

## Description ##

Kwest is a project created for my Bachelor's and for learning purposes. System was designed to have n-tier architecture <br/>
and has been divided into following layers:
* Presentation Layer
    It consists of two client apps, web app created in .NET MVC 5 and Android Nougat app made using Xamarin Android.
* Middle Layer
    It's a library that allows for an easy connection with API (with a DataAccess Layer). This layers mediatets between
    Presentation and DataAccess Layers.
* DataAccess Layer <br />
   It's divided into two sublayers:
   * API (authorization and resources are separated)
   * Library (repository) that makes querying DB easier (currently using LINQ-to-SQL)
* Data Layer
    It consists of ORM(EF6) model classes and of course a database, at the time of writing it's a LocalDB but it may change in the future.

## Demo ##

To be added </br>
Not sure where I want to host it (must be free, thats for sure). Propably going to host it on Firebase </br>
(need to implement new DataAccess Layer) or Azure (need to mock DataAccess Layer since their DB is not free).

## How to launch ##

You can download it and if You have Visual Studio then You're all good to go. </br>
You may (and even should) experience errors when using payments and notifications because I've reset all the secret keys. </br>
Everything should work just fine when I'm done with hosting, for now, sorry for inconvenience. 

## Screenshots ##

### Main page ###
![Main page](README-pics/main-page.png)

### All collections view ###
![All collections display and search form](README-pics/collections-view.png)

### Register form ###
![Register form](README-pics/register-view.png)
![Register form validation errors](README-pics/register-view-validation.png)

### User achievements view ###
![User achievements](README-pics/achievements-view.png)

## Features ##

* Create money collection
* Donate to a started collection
* Create and account
* Login
* Logout
* Show all collections
* Show only finished collections
* Search for a collection (case insensitive search among collections title and descriptions)
* Sort collections (by a date and by a collected money)

### Gamification related features: ###
* Show achievements
* Finish a task (right now there are donation based and login based tasks)
* Get a repeatable/one-time achievement
* Show users ranking
* Pick a collection (picked collections are being displaied on homepage)
* Free payment (a free donation which should be covered by a system owner)
