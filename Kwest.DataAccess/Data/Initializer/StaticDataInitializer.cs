﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;

namespace Kwest.DataAccess.Data.Initializer
{
    public class StaticDataInitializer : DropCreateDatabaseIfModelChanges<DBContext>
    {
        AchievementRewardMapper rewards;
        AchievementMapper achievements;

        public StaticDataInitializer()
        {
            this.rewards = new AchievementRewardMapper();
            this.achievements = new AchievementMapper();
        }

        protected override void Seed(DBContext context)
        {
            this.SeedStatuses(context);
            this.SeedRewards(context);
            this.SeedActions(context);
            this.SeedAchievements(context);

            context.SaveChanges();
            base.Seed(context);
        }

        private void SeedStatuses(DBContext context)
        {
            context.AchievementStatuses.AddOrUpdate(new Model.AchievementStatus[]
            {
                AchievementStatusEnum.Achieved,
                AchievementStatusEnum.Finished,
                AchievementStatusEnum.InProgress
            });
        }

        private void SeedRewards(DBContext context)
        {
            context.Rewards.AddOrUpdate(new[]
            {
                this.rewards.Map(AchievementRewardEnum.FreePayment),
                this.rewards.Map(AchievementRewardEnum.PickCollection)
            });
        }

        private void SeedActions(DBContext context)
        {
            context.AchievementActions.AddOrUpdate(new Model.AchievementAction[]
            {
                AchievementActionEnum.Register,
                AchievementActionEnum.Payment,
                AchievementActionEnum.Login
            });
        }

        private void SeedAchievements(DBContext context)
        {
            context.Achievements.AddOrUpdate(new[]
            {
                achievements.Map(AchievementEnum.HelloKwest),
                achievements.Map(AchievementEnum.ThePatron),
                achievements.Map(AchievementEnum.ICanHelpTooI),
                achievements.Map(AchievementEnum.ICanHelpTooII),
                achievements.Map(AchievementEnum.StillCheckingByI),
                achievements.Map(AchievementEnum.StillCheckingByII)
            });
        }
    }
}
