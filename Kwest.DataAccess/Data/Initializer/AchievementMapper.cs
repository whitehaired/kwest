﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;
using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess.Data.Initializer
{
    public class AchievementMapper
    {
        private Dictionary<AchievementEnum, Achievement> achievementsMap;

        public AchievementMapper()
        {
            this.CreateMap();
        }

        private void CreateMap()
        {
            achievementsMap = new Dictionary<AchievementEnum, Achievement>()
            {
                {
                    AchievementEnum.HelloKwest,
                    new Achievement()
                    {
                        AchievementId = (int)AchievementEnum.HelloKwest,
                        Type = AchievementEnum.HelloKwest.ToString(),
                        Title = "Hello Kwest!!!",
                        Description = "You have became a part of one great community. Welcome!",
                        RequiredRepetitions = 1,
                        RewardId = (int)AchievementRewardEnum.FreePayment,
                        AchievementActionId = (int)AchievementActionEnum.Register,
                        Points = 20,
                        IsRepetable = false
                    }
                },
                {
                    AchievementEnum.ThePatron,
                    new Achievement()
                    {
                        AchievementId = (int)AchievementEnum.ThePatron,
                        Type = AchievementEnum.ThePatron.ToString(),
                        Title = "The... Patron.",
                        Description = "It's your first. Hopefully not the last. We are greateful for your support.",
                        RequiredRepetitions = 1,
                        RewardId = (int)AchievementRewardEnum.FreePayment,
                        AchievementActionId = (int)AchievementActionEnum.Payment,
                        Points = 30,
                        IsRepetable = false
                    }
                },
                {
                    AchievementEnum.ICanHelpTooI,
                    new Achievement()
                    {
                        AchievementId = (int)AchievementEnum.ICanHelpTooI,
                        Type = AchievementEnum.ICanHelpTooI.ToString(),
                        Title = "I can help too I",
                        Description = "Yes! You! Can! And You're killing it!",
                        RequiredRepetitions = 5,
                        RewardId = (int)AchievementRewardEnum.PickCollection,
                        AchievementActionId = (int)AchievementActionEnum.Payment,
                        Points = 10,
                        IsRepetable = true
                    }
                },
                {
                    AchievementEnum.ICanHelpTooII,
                    new Achievement()
                    {
                        AchievementId = (int)AchievementEnum.ICanHelpTooII,
                        Type = AchievementEnum.ICanHelpTooII.ToString(),
                        Title = "I can help too II",
                        Description = "You are saving lives over here!!!",
                        RequiredRepetitions = 20,
                        RewardId = (int)AchievementRewardEnum.FreePayment,
                        AchievementActionId = (int)AchievementActionEnum.Payment,
                        Points = 50,
                        IsRepetable = true
                    }
                },
                {
                    AchievementEnum.StillCheckingByI,
                    new Achievement()
                    {
                        AchievementId = (int)AchievementEnum.StillCheckingByI,
                        Type = AchievementEnum.StillCheckingByI.ToString(),
                        Title = "I'm still here, checking by I",
                        Description = "I'm doing it my way and still helping!",
                        RequiredRepetitions = 10,
                        RewardId = (int)AchievementRewardEnum.PickCollection,
                        AchievementActionId = (int)AchievementActionEnum.Login,
                        Points = 10,
                        IsRepetable = true
                    }
                },
                {
                    AchievementEnum.StillCheckingByII,
                    new Achievement()
                    {
                        AchievementId = (int)AchievementEnum.StillCheckingByII,
                        Type = AchievementEnum.StillCheckingByII.ToString(),
                        Title = "I'm still here, checking by I",
                        Description = "I'm doing it my way and still helping!",
                        RequiredRepetitions = 30,
                        RewardId = (int)AchievementRewardEnum.FreePayment,
                        AchievementActionId = (int)AchievementActionEnum.Login,
                        Points = 40,
                        IsRepetable = true
                    }
                }
            };
        }

        public Achievement Map(AchievementEnum @enum)
        {
            if (achievementsMap.TryGetValue(@enum, out Achievement result))
            {
                return result;
            }
            throw new ArgumentException("");
        }
    }
}
