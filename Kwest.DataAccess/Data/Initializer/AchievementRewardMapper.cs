﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;
using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess.Data.Initializer
{
    public class AchievementRewardMapper
    {
        private Dictionary<AchievementRewardEnum, Reward> rewardsMap;

        public AchievementRewardMapper()
        {
            this.CreateMap();
        }

        private void CreateMap()
        {
            this.rewardsMap = new Dictionary<AchievementRewardEnum, Reward>()
            {
                {
                    AchievementRewardEnum.FreePayment,
                    new Reward()
                    {
                        RewardId = (int)AchievementRewardEnum.FreePayment,
                        Type = AchievementRewardEnum.FreePayment.ToString(),
                        Title = "Free Payment",
                        Description = "Pick a collection! Donate! And we got You covered!"
                    }
                },
                {
                    AchievementRewardEnum.PickCollection,
                    new Reward()
                    {
                        RewardId = (int)AchievementRewardEnum.PickCollection,
                        Type = AchievementRewardEnum.PickCollection.ToString(),
                        Title = "Pick Collection",
                        Description = "Make everybody see it!"
                    }
                }
            };
        }

        public Reward Map(AchievementRewardEnum @enum)
        {
            if (rewardsMap.TryGetValue(@enum, out Reward result))
            {
                return result;
            }
            throw new ArgumentException("");
        }
    }
}
