﻿using Kwest.DataAccess.Data.Model;
using System;
using System.IO;
using System.Data.Entity;

using Microsoft.AspNet.Identity.EntityFramework;

using Kwest.DataAccess.Data.Initializer;

namespace Kwest.DataAccess.Data
{
    public class DBContext : IdentityDbContext<User>
    {
        public DBContext() : base(@"Data Source=(LocalDb)\MSSQLLocalDB;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\b.mdf", throwIfV1Schema: false)
        {
            var solutionRoot = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName;
            var dbPath = Path.Combine(solutionRoot, @"Kwest.DataAccess\App_Data", "dbName.mdf");
            this.Database.Connection.ConnectionString = $@"Data Source=(LocalDb)\MSSQLLocalDB;AttachDBFilename={dbPath};Integrated Security=SSPI;Initial Catalog=A";
            Database.SetInitializer(new StaticDataInitializer());
            // this.Database.Connection.ConnectionString = @"Data Source=(LocalDb)\MSSQLLocalDB;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\b.mdf";
        }

        public DbSet<Collection> Collections { get; set; }
        public DbSet<PaymentOrder> Payments { get; set; }

        public DbSet<AchievementProgress> AchievementProgresses { get; set; }
        public DbSet<AchievementStatus> AchievementStatuses { get; set; }
        public DbSet<AchievementAction> AchievementActions { get; set; }
        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<Reward> Rewards { get; set; }
    }
}
