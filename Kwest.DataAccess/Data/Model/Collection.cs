﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Kwest.DataAccess.Data.Model
{
    public class Collection
    {
        public string CollectionID { get; set; }

        #region title-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public string Title { get; set; }

        #region money-needed-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public decimal? MoneyNeeded { get; set; }

        public decimal MoneyCollected { get; set; }

        #region begin-date-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public DateTime? Begins { get; set; }

        #region finish-date-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public DateTime? Ends { get; set; }

        #region mainimage-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public byte[] MainImage { get; set; }

        #region description-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public string Description { get; set; }

        #region mainvideo-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public byte[] MainVideo { get; set; }

        public string OwnerId { get; set; }
        public User Owner { get; set; }

        public bool isFinished() => this.Ends.Value.Date < DateTime.Now.Date;
    }
}
