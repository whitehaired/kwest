﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Collections.Generic;

namespace Kwest.DataAccess.Data.Model
{
    public class User : IdentityUser
    {
        #region username-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public override string UserName { get => base.UserName; set => base.UserName = value; }

        #region email-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public override string Email { get => base.Email; set => base.Email = value; }

        #region nickname-attributes
        [Required(AllowEmptyStrings = false)]
        #endregion
        public string Nickname { get; set; }
        public string DeviceToken { get; set; }

        public long Points { get; set; }

        public ICollection<Collection> Collections { get; set; }

        [ForeignKey("PickedCollection")]
        public string PickedCollectionId { get; set; }
        public Collection PickedCollection { get; set; }
    }
}