﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;

namespace Kwest.DataAccess.Data.Model
{
    public class AchievementAction
    {
        public int AchievementActionId { get; set; }
        public string Action { get; set; }

        public static implicit operator AchievementActionEnum(AchievementAction action) =>
            (AchievementActionEnum)action.AchievementActionId;

        public static implicit operator AchievementAction(AchievementActionEnum action) =>
            new AchievementAction() { AchievementActionId = (int)action, Action = action.ToString() };
    }
}
