﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;

namespace Kwest.DataAccess.Data.Model
{
    public class Achievement
    {
        public int AchievementId { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int RequiredRepetitions { get; set; }
        public bool IsRepetable { get; set; }

        public int RewardId { get; set; }
        public Reward Reward { get; set; }
        public int Points { get; set; }

        public int AchievementActionId { get; set; }
        public AchievementAction AchievementAction { get; set; }

        public static implicit operator AchievementEnum(Achievement achievement) => (AchievementEnum)achievement.AchievementId;
    }
}
