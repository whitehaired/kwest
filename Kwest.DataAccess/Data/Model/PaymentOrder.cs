﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.DataAccess.Data.Model
{
    public class PaymentOrder
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }

        public string APIID { get; set; }

        public string UserID { get; set; }
        public User User { get; set; }
     
        public string CollectionID { get; set; }
        public Collection Collection { get; set; }
    }
}
