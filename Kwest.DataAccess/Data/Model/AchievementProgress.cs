﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.DataAccess.Data.Model
{
    public class AchievementProgress
    {
        public int AchievementProgressId { get; set; }
        public int Progress { get; set; }
        public DateTime? ProgressAchievedDate { get; set; }

        public int AchievementId { get; set; }
        public Achievement Achievement { get; set; }

        public int AchievementStatusId { get; set; }
        public AchievementStatus AchievementStatus { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }
    }
}
