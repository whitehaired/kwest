﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;

namespace Kwest.DataAccess.Data.Model
{
    public class AchievementStatus
    {
        public int AchievementStatusId { get; set; }
        public string Status { get; set; }

        public static implicit operator AchievementStatusEnum(AchievementStatus status) =>
            (AchievementStatusEnum)status.AchievementStatusId;

        public static implicit operator AchievementStatus(AchievementStatusEnum status) =>
            new AchievementStatus() { AchievementStatusId = (int)status, Status = status.ToString() };

    }
}
