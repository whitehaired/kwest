﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;

namespace Kwest.DataAccess.Data.Model
{
    public class Reward
    {
        public int RewardId { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public static explicit operator AchievementRewardEnum(Reward reward) => (AchievementRewardEnum)reward.RewardId;
    }
}
