﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;

using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly FirebaseMessaging messaging;

        public NotificationRepository()
        {
            var root = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName;
            var privateKeyPath = Path.Combine(root, @"Kwest.DataAccess", @"kwest-0217-firebase-adminsdk-c8f8x-edfde05412.json");
            var app = FirebaseApp.DefaultInstance;
            if(app == null)
            {
                app = FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.FromFile(privateKeyPath).CreateScoped("https://www.googleapis.com/auth/firebase.messaging")
                });
            }
            this.messaging = FirebaseMessaging.GetMessaging(app);
        }

        public async Task Send(User user, string title, string body) =>
            await messaging.SendAsync(CreateNotification(title, body, user.DeviceToken));

        private Message CreateNotification(string title, string body, string token) =>
            new Message()
            {
                Token = token,
                Notification = new Notification()
                {
                    Body = body,
                    Title = title
                }
            };
    }
}
