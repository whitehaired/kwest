﻿using Microsoft.AspNet.Identity;

using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess
{
    public interface IAuthRepository
    {
        IdentityResult Register(User user, string password);
        bool Login(string username, string password);
    }
}
