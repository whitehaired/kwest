﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess
{
    public interface IPaymentRepository
    {
        void Add(PaymentOrder model);
        void Book(string orderId);
        void Book(PaymentOrder payment);
        void Delete(string orderId);
        PaymentOrder Get(string orderId);
    }
}
