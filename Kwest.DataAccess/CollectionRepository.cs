﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Kwest.DataAccess.Data;
using Kwest.DataAccess.Data.Model;
using Kwest.Core;
using Kwest.ViewModels.Collection;

namespace Kwest.DataAccess
{
    public class CollectionRepository : ICollectionRepository
    {
        DBContext context;

        public CollectionRepository(DBContext context)
        {
            this.context = context;
        }

        public void Add(Collection collection)
        {
            context.Collections.Add(collection);
            context.SaveChanges();
        }

        public Collection FindByTitle(string title) =>
            context.Collections
            .Where(collection => title.Equals(collection.Title, StringComparison.OrdinalIgnoreCase))
            .FirstOrDefault();

        public IEnumerable<Collection> Get(CollectionFindViewModel criteria)
        {
            var query = this.context.Collections.Include(collection => collection.Owner);

            if (criteria.OnlyFinished)
            {
                query = query.Where(collection => collection.Ends < DateTime.Now);
            }

            if (!String.IsNullOrEmpty(criteria.SearchPhrase))
            {
                query = query.Where(collection => collection.Title.Contains(criteria.SearchPhrase) || collection.Description.Contains(criteria.SearchPhrase));
            }

            switch (criteria.OrderCategory)
            {
                case OrderCategory.Default:
                    query = query.OrderBy(collection => collection.Ends < DateTime.Now);
                    break;
                case OrderCategory.MoneyLeftAscending:
                    query = query.OrderBy(collection => collection.MoneyNeeded - collection.MoneyCollected);
                    break;
                case OrderCategory.MoneyLeftDescending:
                    query = query.OrderByDescending(collection => collection.MoneyNeeded - collection.MoneyCollected);
                    break;
                case OrderCategory.TimeLeftAscending:
                    query = query.OrderBy(collection => DbFunctions.DiffDays(collection.Ends, collection.Begins));
                    break;
                case OrderCategory.TimeLeftDescending:
                    query = query.OrderByDescending(collection => DbFunctions.DiffDays(collection.Ends, collection.Begins));
                    break;
            }

            return query;
        }

        public Collection Get(string Id) =>
            context.Collections
            .Where(collection => collection.CollectionID == Id)
            .Include(collection => collection.Owner)
            .FirstOrDefault();

        public IEnumerable<Collection> GetPickedCollections() =>
            this.context.Users
                .Where(user => user.PickedCollectionId != null)
                .Include(user => user.PickedCollection)
                .Select(user => user.PickedCollection)
                .Include(collection => collection.Owner);
    }
}
