﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.Core;
using Kwest.DataAccess.Data;
using Kwest.DataAccess.Data.Model;
using Kwest.DataAccess.ExtensionMethods;

namespace Kwest.DataAccess
{
    public class GameRepository : IGameRepository
    {
        DBContext context;
        
        public GameRepository()
        {
            context = new DBContext();
        }

        #region start-game
        public void StartGame(User user)
        {
            this.context.Users.Attach(user);
            var helloKwest = this.context.Achievements.Find((int)AchievementEnum.HelloKwest);
            user.Points += helloKwest.Points;

            var inProgressStatus = (int)AchievementStatusEnum.InProgress;
            var userId = user.Id;
            context.AchievementProgresses.AddRange(new[]
            {
                new AchievementProgress()
                {
                    Progress = 1,
                    AchievementId = (int)AchievementEnum.HelloKwest,
                    AchievementStatusId = (int)AchievementStatusEnum.Achieved,
                    UserId = userId
                },
                new AchievementProgress()
                {
                    Progress = 0,
                    AchievementId = (int)AchievementEnum.ThePatron,
                    AchievementStatusId = inProgressStatus,
                    UserId = userId
                },
                new AchievementProgress()
                {
                    Progress = 0,
                    AchievementId = (int)AchievementEnum.ICanHelpTooI,
                    AchievementStatusId = inProgressStatus,
                    UserId = userId
                },
                new AchievementProgress()
                {
                    Progress = 0,
                    AchievementId = (int)AchievementEnum.ICanHelpTooII,
                    AchievementStatusId = inProgressStatus,
                    UserId = userId
                },
                new AchievementProgress()
                {
                    Progress = 0,
                    AchievementId = (int)AchievementEnum.StillCheckingByI,
                    AchievementStatusId = inProgressStatus,
                    UserId = userId
                },
                new AchievementProgress()
                {
                    Progress = 0,
                    AchievementId = (int)AchievementEnum.StillCheckingByII,
                    AchievementStatusId = inProgressStatus,
                    UserId = userId
                }
            });

            context.SaveChanges();
        }
        #endregion

        #region notify-payment
        public void NotifyPaymentDone(string userId)
        {
            this.GetByAction(userId, AchievementActionEnum.Payment)
                .ToList()
                .ForEach(Count);

            context.SaveChanges();
        }
        #endregion

        #region notify-login
        public void NotifyLogin(string userId)
        {
            var now = DateTime.Now.Date;
            var result = this.GetByAction(userId, AchievementActionEnum.Login).ToList();
            result.ForEach(progress => this.Count(progress, now.AddDays(-1), now));

            context.SaveChanges();
        }
        #endregion

        private IEnumerable<AchievementProgress> GetByAction(string userId, AchievementActionEnum action) =>
            context.AchievementProgresses
                .Where(progress => progress.UserId == userId)
                .Include(progress => progress.Achievement.AchievementAction)
                .Include(progress => progress.AchievementStatus)
                .AsEnumerable()
                .Where(progress => progress.Achievement.AchievementAction.AchievementActionId == (int)action);

        private void Count(AchievementProgress progress, DateTime bottomLimit, DateTime topLimit)
        {
            var achievedDate = progress.ProgressAchievedDate;
            if (!achievedDate.HasValue || achievedDate.Value.IsInBetween(bottomLimit, topLimit, DateTimeCompareEnum.LeftSideInclusive))
            {
                progress.ProgressAchievedDate = topLimit;
                this.Count(progress);
            }
            else if(achievedDate < bottomLimit)
            {
                progress.ProgressAchievedDate = topLimit;
                progress.Progress = 1;
            }
        }

        private void Count(AchievementProgress progress)
        {
            if (progress.AchievementStatus == AchievementStatusEnum.InProgress)
            {
                progress.Progress++;
                if (progress.Progress == progress.Achievement.RequiredRepetitions)
                {
                    progress.AchievementStatusId = (int)AchievementStatusEnum.Achieved;
                 
                    var user = this.context.Users.Find(progress.UserId);
                    user.Points += progress.Achievement.Points;
                }
            }
        }

        #region get-achievements
        public IEnumerable<AchievementProgress> UserAchievements(string userId) =>
            context.AchievementProgresses
                .Where(progresses => progresses.UserId == userId)
                .Include(progresses => progresses.AchievementStatus)
                .Include(progresses => progresses.Achievement.Reward);
        #endregion

        #region free-payment
        public void FreePayment(string collectionId, string userId, decimal amount)
        {
            var collection = this.context.Collections.Find(collectionId);
            collection.MoneyCollected += amount;

            var achievementProgress = this.FindAchievedProgress(userId, AchievementRewardEnum.FreePayment);

            if (achievementProgress != null)
            {
                if (achievementProgress.Achievement.IsRepetable)
                {
                    achievementProgress.AchievementStatusId = (int)AchievementStatusEnum.InProgress;
                    achievementProgress.Progress = 0;
                }
                else
                {
                    achievementProgress.AchievementStatusId = (int)AchievementStatusEnum.Finished;
                }
            }

            this.context.SaveChanges();
        }
        #endregion

        #region pick-collection
        public void Pick(string collectionId, string userId)
        {
            var user = this.context.Users.Find(userId);
            user.PickedCollectionId = collectionId;

            var achievementProgress = this.FindAchievedProgress(userId, AchievementRewardEnum.PickCollection);

            if (achievementProgress != null)
            {
                if (achievementProgress.Achievement.IsRepetable)
                {
                    achievementProgress.AchievementStatusId = (int)AchievementStatusEnum.InProgress;
                    achievementProgress.Progress = 0;
                }
                else
                {
                    achievementProgress.AchievementStatusId = (int)AchievementStatusEnum.Finished;
                }
            }

            this.context.SaveChanges();
        }
        #endregion

        private AchievementProgress FindAchievedProgress(string userId, AchievementRewardEnum reward) =>
            this.context.AchievementProgresses
                .Where(progress => progress.UserId == userId)
                .Where(progress => progress.AchievementStatusId == (int)AchievementStatusEnum.Achieved)
                .Include(progress => progress.Achievement)
                .Where(progress => progress.Achievement.RewardId == (int)reward)
                .OrderByDescending(progress => progress.Achievement.IsRepetable)
                .FirstOrDefault();

        #region users-ranking
        public IEnumerable<User> Ranking() =>
            this.context.Users
                .OrderByDescending(user => user.Points)
                .Take(100);
        #endregion
    }
}
