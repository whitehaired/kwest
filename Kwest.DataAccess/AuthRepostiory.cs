﻿using Microsoft.AspNet.Identity;

using Kwest.DataAccess.Data.Model;
using Microsoft.AspNet.Identity.Owin;

namespace Kwest.DataAccess
{
    public class AuthRepostiory : IAuthRepository
    {
        private UserManager<User> userManager;

        public AuthRepostiory(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        public bool Login(string username, string password)
        {
            var user = userManager.Find(username, password);
            return user != null;
        }

        public IdentityResult Register(User user, string password)
        {
            return userManager.CreateAsync(user, password).Result;
        }
    }
}
