﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess
{
    public interface IUserRepository
    {
        User FindByNickname(string nickname);
        User FindByEmail(string email);
        User FindByUsername(string username);
        User FindById(string id);
        bool HasPickCollectionReward(string userId);
        IEnumerable<Reward> GetUserRewards(string userId);
        Collection GetUsersPickedCollection(string userId);
        bool UpdateDeviceKey(string userId, string deviceKey);
    }
}
