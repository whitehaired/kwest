﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess
{
    public interface INotificationRepository
    {
        Task Send(User user, string title, string message);
    }
}
