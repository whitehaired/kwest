﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Kwest.DataAccess.Data;
using Kwest.DataAccess.Data.Model;
using Kwest.Core;

namespace Kwest.DataAccess
{
    public class UserRepository : IUserRepository
    {
        DBContext context;

        public UserRepository(DBContext context)
        {
            this.context = context;
        }

        public User FindByNickname(string nickname) =>
            context.Users
            .Where(user => user.Nickname.Equals(nickname))
            .FirstOrDefault();

        public User FindByEmail(string email) =>
            context.Users
            .Where(user => user.Email.Equals(email))
            .FirstOrDefault();

        public User FindByUsername(string username) =>
            context.Users
            .Where(user => user.UserName.Equals(username))
            .FirstOrDefault();

        public User FindById(string id) => this.context.Users.Find(id);

        public bool HasPickCollectionReward(string userId) =>
            this.GetUserRewards(userId)
                .Any(reward => reward.RewardId == (int)AchievementRewardEnum.FreePayment);

        public IEnumerable<Reward> GetUserRewards(string userId) =>
            this.context.AchievementProgresses
                .Where(progress => progress.UserId == userId)
                .Where(progress => progress.AchievementStatusId == (int)AchievementStatusEnum.Achieved)
                .Include(progress => progress.Achievement.Reward)
                .Select(progress => progress.Achievement.Reward);

        public Collection GetUsersPickedCollection(string userId) =>
            this.context.Users
                .Where(user => user.Id == userId)
                .Include(user => user.PickedCollection)
                .Select(user => user.PickedCollection)
                .FirstOrDefault();

        public bool UpdateDeviceKey(string userId, string deviceKey)
        {
            var user = this.FindById(userId);
            if(user != null)
            {
                user.DeviceToken = deviceKey;
                this.context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
