﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.DataAccess.ClientApplications
{
    public class ClientApplication
    {
        public ClientApplication(string id, string secret)
        {
            this.ID = id;
            this.Secret = secret;
        }

        public string ID { get; private set; }
        public string Secret { get; private set; }
    }
}
