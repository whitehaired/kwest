﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.DataAccess.ClientApplications
{
    public class ClientApplicationStore : IClientApplicationStore
    {
        Dictionary<string, ClientApplication> clientApplications;

        public ClientApplicationStore()
        {
            clientApplications = new Dictionary<string, ClientApplication>();
            this.Seed();
        }

        private void Seed()
        {
            var mobileApp = new ClientApplication(id: "ZoQYBlEXpTBmBqbYWxDYEz72T6Y5OIrY", secret: "WMvqT7aO404ScSnpbtQ3lhclJe3o3e9mg6hBRPHZpQ2");
            var webApp = new ClientApplication(id: "nxGEKXrtnWZkyOFe4lgPtWERne2xEnr5", secret: "hZic49vbKsNX05NNx0CKJuKT0UBvBfWAAxBvSLHmfUm");

            clientApplications.Add(mobileApp.ID, mobileApp);
            clientApplications.Add(webApp.ID, webApp);
        }

        public ClientApplication FindClientAppById(string id)
        {
            return clientApplications.Where(pair => pair.Key == id).Select(pair => pair.Value).FirstOrDefault();
        }
    }
}
