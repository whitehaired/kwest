﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Kwest.DataAccess.Data;
using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess
{
    public class PaymentRepository : IPaymentRepository
    {
        DBContext context;

        public PaymentRepository(DBContext context)
        {
            this.context = context;
        }

        public void Add(PaymentOrder model)
        {
            context.Payments.Add(model);
            context.SaveChanges();
        }

        public void Book(string orderId)
        {
            var payment = this.Get(orderId);
            //var amount = payment.Amount;
            this.Book(payment);
            //payment.Collection.MoneyCollected += payment.Amount;
            //context.Payments.Remove(payment);
            //context.SaveChanges();
        }

        public PaymentOrder Get(string orderId)
        {
            return context.Payments.Where(order => order.Id == orderId)
                .Include(order => order.Collection)
                .FirstOrDefault();
        }

        public void Book(PaymentOrder payment)
        {
            payment.Collection.MoneyCollected += payment.Amount;
            context.Payments.Remove(payment);
            context.SaveChanges();
        }

        public void Delete(string orderId)
        {
            var payment = context.Payments.Find(orderId);
            context.Payments.Remove(payment);
            context.SaveChanges();
        }
    }
}
