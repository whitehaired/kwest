﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.DataAccess.Data.Model;

namespace Kwest.DataAccess
{
    public interface IGameRepository
    {
        void StartGame(User user);
        void NotifyPaymentDone(string userId);
        void NotifyLogin(string userId);
        IEnumerable<AchievementProgress> UserAchievements(string userId);
        void FreePayment(string collectionId, string userId, decimal amount);
        void Pick(string collectionId, string userId);
        IEnumerable<User> Ranking();
    }
}
