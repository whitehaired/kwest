﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.DataAccess.ExtensionMethods
{
    public static class DateTimeExtensionMethods
    {
        public static bool IsInBetween(this DateTime toCheck, DateTime bottomLimit, DateTime topLimit, DateTimeCompareEnum compare)
        {
            switch (compare)
            {
                case DateTimeCompareEnum.Exclusive:
                    return toCheck > bottomLimit && toCheck < topLimit;
                case DateTimeCompareEnum.Inclusive:
                    return toCheck >= bottomLimit && toCheck <= topLimit;
                case DateTimeCompareEnum.LeftSideInclusive:
                    return toCheck >= bottomLimit && toCheck < topLimit;
                case DateTimeCompareEnum.RightSideInclusive:
                    return toCheck > bottomLimit && toCheck <= topLimit;
            }
            throw new ArgumentException("DateTimeCompareEnum wrong value");
        }
    }
}
