﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.DataAccess.ExtensionMethods
{
    public enum DateTimeCompareEnum
    {
        Inclusive,
        Exclusive,
        LeftSideInclusive,
        RightSideInclusive
    }
}
