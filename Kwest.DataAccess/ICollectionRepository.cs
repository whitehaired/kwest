﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Collection;

namespace Kwest.DataAccess
{
    public interface ICollectionRepository
    {
        void Add(Collection collection);

        Collection FindByTitle(string title);

        IEnumerable<Collection> Get(CollectionFindViewModel criteria);

        Collection Get(string Id);

        IEnumerable<Collection> GetPickedCollections();
    }
}
