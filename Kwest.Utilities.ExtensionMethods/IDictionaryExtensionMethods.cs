﻿using System.Collections.Generic;

namespace Kwest.Utilities.ExtensionMethods
{
    public static class IDictionaryExtensionMethods
    {
        public static IEnumerable<T> GetValueOrEmpty<TKey, T>(this IDictionary<TKey, IEnumerable<T>> dictionary, TKey key)
        {
            return dictionary.ContainsKey(key) ? dictionary[key] : new T[] { };
        }
    }
}
