﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Attribute
{
    public class DateGreaterThanProperty : ValidationAttribute
    {
        private string propertyName;

        public DateGreaterThanProperty(string propertyName)
        {
            this.propertyName = propertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var providedProperty = this.GetProperty(validationContext);
            var providedPropertyValue = this.GetPropertyValue(validationContext.ObjectInstance, providedProperty);

            var dateTimeValue = value as DateTime?;
            if (dateTimeValue.HasValue)
            {
                if(dateTimeValue > providedPropertyValue)
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult(this.ErrorMessage);
            }
            throw new ArgumentException("Attribute can be used only with DateTime values");
        }

        protected virtual PropertyInfo GetProperty(ValidationContext validationContext)
        {
            var property = validationContext.GetType().GetProperty(this.propertyName);

            if (property != null)
            {
                return property;
            }
            throw new ArgumentException("Provided property name is invalid");
        }

        protected virtual DateTime GetPropertyValue(object containingObject, PropertyInfo property)
        {
            var value = property.GetValue(containingObject) as DateTime?;
            if(value.HasValue)
            {
                return value.Value;
            }
            throw new ArgumentException("Provided property name doesn't reference DateTime value");
        }
    }
}
