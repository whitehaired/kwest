﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Attribute
{
    public class StringEquals : ValidationAttribute
    {
        private string[] expectedValues;

        public StringEquals(string[] expectedValues)
        {
            this.expectedValues = expectedValues;
        }

        public override bool IsValid(object value)
        {
            var stringValue = value.ToString();
            return !String.IsNullOrEmpty(stringValue) && expectedValues.Contains(stringValue);
        }
    }
}
