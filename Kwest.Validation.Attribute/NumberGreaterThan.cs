﻿using Kwest.Validation.Attribute.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Attribute
{
    public class NumberGreaterThan : ValidationAttribute
    {
        private double limit;

        public NumberGreaterThan(double limit)
        {
            this.limit = limit;
        }

        public override bool IsValid(object value)
        {
            if (value.IsNumeric())
            {
                return Convert.ToDecimal(value) > Convert.ToDecimal(limit);
            }
            throw new ArgumentException("Attribute can be used only with numeric data types");
        }
    }
}
