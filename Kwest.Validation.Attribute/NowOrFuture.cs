﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Attribute
{
    public class NowOrFuture : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var datetimeValue = value as DateTime?;
            if (datetimeValue.HasValue)
            {
                return Convert.ToDateTime(value).Date >= DateTime.Now.Date;
            }
            throw new ArgumentException("Attribute can be used only with DateTime values");
        }
    }
}
