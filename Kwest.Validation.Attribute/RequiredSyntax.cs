﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Attribute
{
    public class RequiredSyntax : ValidationAttribute
    {
        public bool RequireDigit { get; set; }
        public bool RequireSpecialCharacter { get; set; }
        public bool RequireUppercase { get; set; }
        public bool RequireLowercase { get; set; }
        public string SpecialCharacters { get; set; } = "?!@#$%&*";

        public override bool IsValid(object value)
        {
            if (value is string)
            {
                bool containsDigit = !RequireDigit;
                bool containsSpecialCharacter = !RequireSpecialCharacter;
                bool containsUppercase = !RequireUppercase;
                bool containsLowercase = !RequireLowercase;

                foreach (var letter in value as string)
                {
                    if (!containsDigit && Char.IsDigit(letter))
                    {
                        containsDigit = true;
                    }
                    if (!containsSpecialCharacter && this.IsSpecialCharacter(letter))
                    {
                        containsSpecialCharacter = true;
                    }
                    if (!containsUppercase && Char.IsUpper(letter))
                    {
                        containsUppercase = true;
                    }
                    if (!containsLowercase && Char.IsLower(letter))
                    {
                        containsLowercase = true;
                    }
                }

                return containsDigit && containsSpecialCharacter && containsUppercase && containsLowercase;
            }
            return false;
        }

        private bool IsSpecialCharacter(char letter)
        {
            return this.SpecialCharacters.Contains(letter);
        }
    }
}
