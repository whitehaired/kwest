﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Attribute.ExtensionMethods
{
    public static class ObjectExtensions
    {
        public static bool IsNumeric(this object value)
        {
            switch (value)
            {
                case byte b:
                case sbyte sb:
                case short s:
                case ushort us:
                case int i:
                case uint ui:
                case long l:
                case ulong ul:
                case float f:
                case double d:
                case decimal dc:
                    return true;
            }
            return false;
        }
    }
}
