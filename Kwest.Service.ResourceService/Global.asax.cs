using Kwest.Configurations.IoCContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Unity;
using Unity.WebApi;

namespace Kwest.Service.ResourceService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = new UnityContainer();
            DefaultContainerConfigurations.UseCollectionRepository(container);
            DefaultContainerConfigurations.UseClientRepository(container);
            DefaultContainerConfigurations.UsePaymentRepostiory(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
