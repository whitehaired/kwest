﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Kwest.ViewModels.Payment;
using Kwest.DataAccess;
using Kwest.DataAccess.Data.Model;
using Kwest.Configurations.APIRoutes;
using Kwest.ViewModels.Payment.API;
using System.Net.Mail;
using Kwest.Conversion.Mapper;
using Kwest.Conversion;
using Kwest.Conversion.Converter;
using Kwest.Service.ExtensionMethod;

namespace Kwest.Service.ResourceService.Controllers
{
    [RoutePrefix(Payment.Prefix)]
    public class PaymentController : ApiController
    {
        IPaymentRepository payments;
        ICollectionRepository collections;
        IUserRepository users;
        IGameRepository game;
        INotificationRepository notifications;
        private IBaseMapper<PaymentOrder> paymentMapper;
        private IBaseConverter<PaymentViewModel, PaymentOrder> paymentFormConverter;

        public PaymentController(IPaymentRepository payments, ICollectionRepository collections, IUserRepository users)
        {
            this.payments = payments;
            this.collections = collections;
            this.users = users;
            this.game = new GameRepository();
            this.notifications = new NotificationRepository();
            this.paymentMapper = new PaymentOrderMapper();
            this.paymentFormConverter = new PaymentFormConverter();
        }

        #region get-id
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string orderId)
        {
            var paymentOrder = payments.Get(orderId);
            if (paymentOrder != null)
            {
                return Content(HttpStatusCode.OK, this.paymentMapper.Convert(paymentOrder));
            }
            return NotFound();
        }
        #endregion

        #region free-payment-reward
        [HttpPut]
        [Route("free")]
        public IHttpActionResult Free(FreePaymentViewModel model)
        {
            this.Validate(model);
            if (ModelState.IsValid)
            {
                this.game.FreePayment(model.CollectionID, model.UserID, model.Amount);
                this.SendMailNotification(model);
                /*
                 * omitted return, add it and add client handling
                 */
            }
            return BadRequest();
        }

        private void SendMailNotification(FreePaymentViewModel model)
        {
            MailMessage message = new MailMessage()
            {
                From = new MailAddress("siwiec_marek@hotmail.com"),
                Subject = $"Free payment - {model.CollectionID}",
                IsBodyHtml = false,
                Body = $"Reward - free payment - {model.RedirectURL}"
            };
            message.To.Add("siwiec_marek@hotmail.com");
            SmtpClient smtp = new SmtpClient()
            {
                Port = 587,
                Host = "smtp-mail.outlook.com",
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("siwiec_marek@hotmail.com", "undead92"),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            smtp.Send(message);
        }

        private void Validate(FreePaymentViewModel model)
        {
            this.ValidateCollection(model.CollectionID);
            this.ValidateUser(model.UserID);
        }

        private void ValidateCollection(string collectionId)
        {
            var collection = collections.Get(collectionId);
            if(collection == null)
            {
                this.ModelState.AddModelError(String.Empty, "Provided collection id is invalid");
            }
        }
        public void ValidateUser(string userId)
        {
            if(userId == null)
            {
                this.ModelState.AddModelError(String.Empty, "User id is mandatory");
            }
            var user = users.FindById(userId);
            if(user == null)
            {
                this.ModelState.AddModelError(String.Empty, "Provided user id is invalid");
            }
        }
        #endregion

        #region post
        [HttpPost]
        [Route("")]
        public IHttpActionResult Post(PaymentViewModel model)
        {
            this.Validate(model);
            if (ModelState.IsValid)
            {
                payments.Add(this.paymentFormConverter.Convert(model));
                return Ok();
            }
            return Content(HttpStatusCode.BadRequest, this.ModelState.GetErrorDictionary());
        }

        private void Validate(PaymentViewModel model)
        {
            this.ValidateCollection(model);
            this.ValidateUser(model);
        }

        private void ValidateCollection(PaymentViewModel model)
        {
            var collection = this.collections.Get(model.CollectionID);
            if (collection == null)
            {
                this.ModelState.AddModelError(String.Empty, "Provided collectionId is invalid");
            }
            if (!model.CollectionTitle.Equals(model.CollectionTitle))
            {
                this.ModelState.AddModelError(String.Empty, "Provided collection title is invalid");
            }
        }

        private void ValidateUser(PaymentViewModel model)
        {
            if (model.UserLoggedIn)
            {
                if (users.FindById(model.UserID) == null)
                {
                    this.ModelState.AddModelError(String.Empty, "User ID couldn't be found. Please contact support.");
                }
            }
        }
        #endregion

        #region put
        [HttpPut]
        [Route("")]
        public IHttpActionResult Put(string orderId)
        {
            var payment = payments.Get(orderId);
            var userId = payment.UserID;
            // move helper method to paymentorder class
            if (IsFinalPayment(payment))
            {
                var user = users.FindById(payment.Collection.OwnerId);
                if(user.DeviceToken != null)
                {
                    notifications.Send(user, "Money collected!", $"Hi, {user.Nickname}. We got good news! Your collection has been completed. Contact us for collection finalization.");
                }
            }

            payments.Book(payment);
            if (!String.IsNullOrEmpty(userId))
            {
                this.game.NotifyPaymentDone(userId);
            }
            return Ok();
        }

        private bool IsFinalPayment(PaymentOrder payment) =>
            (payment.Amount + payment.Collection.MoneyCollected) == payment.Collection.MoneyNeeded;
        #endregion

        #region delete
        [HttpDelete]
        [Route("")]
        public IHttpActionResult Delete(string orderId)
        {
            // add not found handling
            payments.Delete(orderId);
            return Ok();
        }
        #endregion
    }
}
