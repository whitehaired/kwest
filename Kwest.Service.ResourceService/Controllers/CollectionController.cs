﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Kwest.Core;
using Kwest.DataAccess;
using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Collection;
using Kwest.ViewModels.AchievementReward;
using Kwest.Conversion.Mapper;
using Kwest.Conversion;
using Kwest.Conversion.Converter;
using Kwest.Validation.Validator;
using Kwest.Validation.Validator.CollectionValidator;
using Kwest.Service.ExtensionMethod;

namespace Kwest.Service.ResourceService.Controllers
{
    [RoutePrefix(Kwest.Configurations.APIRoutes.Collection.Prefix)]
    public class CollectionController : ApiController
    {
        ICollectionRepository collections;
        IUserRepository users;
        IGameRepository game;
        private IBaseMapper<IEnumerable<Collection>> collectionIconMapper;
        private IBaseConverter<CollectionFormDTO, Collection> collectionFormConverter;
        private IBaseValidator<CollectionFormDTO> collectionValidator;

        public CollectionController(ICollectionRepository collections, IUserRepository users)
        {
            this.collections = collections;
            this.users = users;
            this.game = new GameRepository();
            this.collectionIconMapper = new CollectionIconMapper();
            this.collectionFormConverter = new CollectionFormConverter();
            this.collectionValidator = new CollectionValidator(this.collections, this.users);
        }

        #region get
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get([FromUri] CollectionFindViewModel criteria)
        {
            var result = this.collections.Get(criteria);
            if (result.Count() == 0)
            {
                return NotFound();
            }
            return Content(HttpStatusCode.OK, this.collectionIconMapper.Convert(result));
        }
        #endregion

        #region get-id
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get(string collectionId, string userId)
        {
            var collection = collections.Get(collectionId);
            if (collection != null)
            {
                var collectionViewModel = this.CreateCollectionViewModel(collection);

                if (userId != null)
                {
                    var userRewards = this.users.GetUserRewards(userId);
                    var pickRewardsAmount = userRewards.Where(reward => reward.RewardId == (int)AchievementRewardEnum.PickCollection).Count();
                    var paymentRewardAmount = userRewards.Where(reward => reward.RewardId == (int)AchievementRewardEnum.FreePayment).Count();

                    var pickedCollection = this.users.GetUsersPickedCollection(userId);

                    collectionViewModel.Rewards = new UserRewardViewModel()
                    {
                        HasPickedCollection = pickedCollection != null,
                        PickedCollectionId = pickedCollection?.CollectionID,
                        PickedCollectionTitle = pickedCollection?.Title,
                        HasPickReward = pickRewardsAmount != 0,
                        PickRewardAmount = pickRewardsAmount,
                        HasFreePaymentReward = paymentRewardAmount != 0,
                        FreePaymentAmount = paymentRewardAmount
                    };
                }

                return Content(HttpStatusCode.OK, collectionViewModel);
            }
            return NotFound();
        }

        private CollectionViewModel CreateCollectionViewModel(Collection collection)
        {
            var now = DateTime.Now.Date;
            var finished = collection.Ends.Value.Date < now;
            return new CollectionViewModel()
            {
                Id = collection.CollectionID,
                Title = collection.Title,
                MoneyNeeded = collection.MoneyNeeded.Value,
                MoneyCollected = collection.MoneyCollected,
                Begins = collection.Begins.Value,
                Ends = collection.Ends.Value,
                Finished = finished,
                TimeLeft = finished ? "Finished" : collection.Ends.Value.Date.Subtract(now).Days.ToString(),
                MainImage = collection.MainImage,
                Description = collection.Description,
                MainVideo = collection.MainVideo,
                OwnerId = collection.OwnerId,
                Owner = collection.Owner.Nickname
            };
        }
        #endregion

        #region create
        [HttpPost]
        [Route("")]
        public IHttpActionResult Create(CollectionFormDTO model)
        {
            this.Validate(model);

            if (ModelState.IsValid)
            {
                var collection = this.collectionFormConverter.Convert(model);
                collections.Add(collection);
                return Ok();
            }
            return Content(HttpStatusCode.BadRequest, this.ModelState.GetErrorDictionary());
        }

        private void Validate(CollectionFormDTO model)
        {
            this.collectionValidator.Validate(model).ToList().ForEach(pair => ModelState.AddModelErrors(pair.Key, pair.Value));
        }
        #endregion

        #region pick-collection
        [HttpPut]
        [Route("")]
        public IHttpActionResult Put(string collectionId, string userId)
        {
            if(this.users.FindById(userId) == null)
            {
                ModelState.AddModelError("UserId", "Specified userId doesn't exist");
            }
            if (!this.users.HasPickCollectionReward(userId))
            {
                ModelState.AddModelError("UserId", "User doesn't have pick reward");
            }
            if(this.collections.Get(collectionId).OwnerId == userId)
            {
                ModelState.AddModelError("UserId", "Owner cannot pick own collection");
            }
            if (ModelState.IsValid)
            {
                this.game.Pick(collectionId, userId);
                return Ok();
            }
            return Content(HttpStatusCode.BadRequest, this.ModelState.GetErrorDictionary());
        }
        #endregion

        #region picked-collections
        [HttpGet]
        [Route("picked")]
        public IHttpActionResult Picked() =>
            Ok(this.collectionIconMapper.Convert(
                    this.collections.GetPickedCollections()
                        .Where(collection => !collection.isFinished())
                        .GroupBy(collection => collection.CollectionID)
                        .Select(group => new {
                            AmountOfPicks = group.Count(),
                            Collection = group.First()
                        })
                        .OrderByDescending(collection => collection.AmountOfPicks)
                        .Select(group => group.Collection))
                );
        #endregion
    }
}
