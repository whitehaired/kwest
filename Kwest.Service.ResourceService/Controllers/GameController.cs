﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Kwest.Conversion.Mapper;
using Kwest.DataAccess;
using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Achievement;
using Kwest.ViewModels.User;

namespace Kwest.Service.ResourceService.Controllers
{
    [RoutePrefix("game")]
    public class GameController : ApiController
    {
        IGameRepository game;
        private IBaseMapper<IEnumerable<AchievementProgress>> achievementMapper;
        private IBaseMapper<IEnumerable<User>> rankingMapper;

        public GameController()
        {
            game = new GameRepository();
            this.achievementMapper = new AchievementMapper();
        }

        // add user validation - user not found
        #region user-achievements
        [HttpGet]
        [Route("achievement")]
        public IHttpActionResult GetUserAchievements(string userId) =>
            Content(HttpStatusCode.OK, this.achievementMapper.Convert(game.UserAchievements(userId)));
        #endregion

        #region users-ranking
        [HttpGet]
        [Route("ranking")]
        public IHttpActionResult Ranking() =>
            Content(HttpStatusCode.OK, this.rankingMapper.Convert(this.game.Ranking()));
        #endregion
    }
}
