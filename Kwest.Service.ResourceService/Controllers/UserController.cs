﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Kwest.DataAccess;

namespace Kwest.Service.ResourceService.Controllers
{
    [RoutePrefix("user")]
    public class UserController : ApiController
    {
        IUserRepository users;

        public UserController(IUserRepository users)
        {
            this.users = users;
        }

        [HttpPut]
        [Route("key")]
        public IHttpActionResult Key(string userId, string deviceKey)
        {
            if (users.UpdateDeviceKey(userId, deviceKey))
            {
                return Ok();
            }
            return NotFound();
        }
    }
}