using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

using Unity;
using Unity.WebApi;

using Kwest.Configurations;
using Kwest.Configurations.IoCContainer;

namespace Kwest.Service.AuthorizationService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = new UnityContainer();
            DefaultContainerConfigurations.UseAuthRepository(container);
            DefaultContainerConfigurations.UseClientRepository(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
