﻿using System.Web.Http;
using System.Web.Http.Validation;

namespace Kwest.Service.AuthorizationService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Services.Replace(typeof(IBodyModelValidator), new PrefixlessModelValidator(config.Services.GetBodyModelValidator()));

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
