﻿using System;
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Kwest.Configurations.APIRoutes;
using Kwest.Service.AuthorizationService.Provider;
using Kwest.Service.AuthorizationService.Formats;

[assembly: OwinStartup(typeof(Kwest.Service.AuthorizationService.App_Start.OWINconfig))]
namespace Kwest.Service.AuthorizationService.App_Start
{
    public class OWINconfig
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString($"/{Account.Prefix}/{Account.Actions.Login}"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationProvider(),
                AccessTokenFormat = new JWTTokenFormat()
            });
        }
    }
}
