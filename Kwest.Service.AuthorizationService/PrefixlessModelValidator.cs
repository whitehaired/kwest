﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;
using System.Web.Http.Validation;

namespace Kwest.Service.AuthorizationService
{
    public class PrefixlessModelValidator : IBodyModelValidator
    {
        IBodyModelValidator validator;
        public PrefixlessModelValidator(IBodyModelValidator validator)
        {
            this.validator = validator;
        }

        public bool Validate(object model, Type type, ModelMetadataProvider metadataProvider, HttpActionContext actionContext, string keyPrefix)
        {
            return validator.Validate(model, type, metadataProvider, actionContext, String.Empty);
        }
    }
}