﻿using System;
using System.Linq;
using System.Web.Http;

using Kwest.Configurations.APIRoutes;
using Kwest.DataAccess;
using Kwest.DataAccess.Data.Model;
using Kwest.Service.ExtensionMethod;
using Kwest.ViewModels.Register;
using Kwest.Validation.Validator;
using Kwest.Validation.Validator.UserValidator;
using Kwest.Conversion;
using Kwest.Conversion.Converter;

namespace Kwest.Service.AuthorizationService.Controllers
{
    [RoutePrefix(Account.Prefix)]
    public class AccountController : ApiController
    {
        private IAuthRepository auth;
        private IUserRepository users;
        private IBaseConverter<RegisterFormDTO, User> registerFormConverter;
        private IBaseValidator<RegisterFormDTO> userValidator;

        public AccountController(IAuthRepository auth, IUserRepository users)
        {
            this.auth = auth;
            this.users = users;
            this.registerFormConverter = new RegisterFromConverter();
            this.userValidator = new UserValidator(this.users);
        }

        [HttpPost]
        [Route(Account.Actions.Register)]
        public IHttpActionResult Register(RegisterFormDTO model)
        {
            this.Validate(model);

            if (ModelState.IsValid)
            {
                var user = this.registerFormConverter.Convert(model);
                var result = auth.Register(user, model.Password);
                if (result.Succeeded)
                {
                    new GameRepository().StartGame(user);
                    return Ok();
                }
                this.ModelState.AddModelErrors(String.Empty, result.Errors);
            }
            return Content(System.Net.HttpStatusCode.BadRequest, this.ModelState.GetErrorDictionary());
        }

        private void Validate(RegisterFormDTO user)
        {
            this.userValidator.Validate(user).ToList().ForEach(pair => ModelState.AddModelErrors(pair.Key, pair.Value));
        }        
    }
}
