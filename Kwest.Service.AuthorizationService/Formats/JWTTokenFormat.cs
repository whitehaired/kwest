﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Web;

using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;

using Kwest.DataAccess.ClientApplications;

namespace Kwest.Service.AuthorizationService.Formats
{
    public class JWTTokenFormat : ISecureDataFormat<AuthenticationTicket>
    {
        IClientApplicationStore clientStore;

        public JWTTokenFormat()
        {
            this.clientStore = new ClientApplicationStore();
        }

        public string Protect(AuthenticationTicket data)
        {
            this.ValidateData(data);
            var key = this.GetApplicationSecretKey(data.Properties.Dictionary["clientID"]);

            return this.CreateUserAccessToken(data, key);
        }

        private void ValidateData(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("Data cannot be null");
            }
            else if (!data.Properties.Dictionary.ContainsKey("clientID"))
            {
                throw new InvalidOperationException("Authentication ticket doesn't contain clientID");
            }
        }

        private byte[] GetApplicationSecretKey(string clientAppID) =>
            Encoding.Default.GetBytes(clientStore.FindClientAppById(clientAppID).Secret);
        
        private string CreateUserAccessToken(AuthenticationTicket data, byte[] secretKey)
        {
            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                IssuedAt = data.Properties.IssuedUtc.Value.DateTime,
                Expires = data.Properties.ExpiresUtc.Value.DateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKey), SecurityAlgorithms.HmacSha256),
                Subject = new System.Security.Claims.ClaimsIdentity(data.Identity.Claims)
            };
            var tokenHandler = new JwtSecurityTokenHandler();

            return tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();    // it's for validation on resource server side - gonna implement it after resource server is up and working
        }
    }
}