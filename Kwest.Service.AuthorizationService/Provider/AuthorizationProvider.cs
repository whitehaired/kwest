﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

using Kwest.DataAccess;
using Kwest.DataAccess.Data;
using Kwest.DataAccess.Data.Model;
using Kwest.DataAccess.ClientApplications;

namespace Kwest.Service.AuthorizationService.Provider
{
    public class AuthorizationProvider : OAuthAuthorizationServerProvider
    {
        IAuthRepository auth;
        IUserRepository users;
        IClientApplicationStore clientsStore;
        IGameRepository game;

        public AuthorizationProvider()
        {
            this.auth = new AuthRepostiory(new Microsoft.AspNet.Identity.UserManager<User>(new UserStore<User>(new DBContext())));
            this.users = new UserRepository(new DBContext());
            this.clientsStore = new ClientApplicationStore();
            this.game = new GameRepository();
        }

        public AuthorizationProvider(IAuthRepository authRepo)
        {
            this.auth = authRepo;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            this.GetCredentials(context);
            this.ValidateClientID(context);
            return Task.CompletedTask;
        }

        private void GetCredentials(OAuthValidateClientAuthenticationContext context)
        {
            if (!context.TryGetBasicCredentials(out string _, out string _))
            {
                context.TryGetFormCredentials(out string _, out string _);
            }
        }

        private void ValidateClientID(OAuthValidateClientAuthenticationContext context)
        {
            var clientID = context.ClientId;

            if (clientID == null)
            {
                context.SetError("Invalid client ID", "Client id has not been specified");
            }
            else if (clientsStore.FindClientAppById(clientID) == null)
            {
                context.SetError("Unauthroized client ID", $"Client ID: {context.ClientId} is not valid.");
            }
            else
            {
                context.Validated();
            }
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            // context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });  // commented for now, didn't make any difference - test with resource server
            if (this.ValidateUser(context))
            {
                var user = users.FindByUsername(context.UserName);
                context.Validated(this.CreateAuthenticatedUserTicket(context, user));
            }
            return Task.CompletedTask;
        }
        private bool ValidateUser(OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (auth.Login(context.UserName, context.Password))
            {
                return true;
            }
            context.SetError("invalid_grant", "Provided credentials are invalid");
            return false;
        }
        private AuthenticationTicket CreateAuthenticatedUserTicket(OAuthGrantResourceOwnerCredentialsContext context, User user)
        {
            this.game.NotifyLogin(user.Id);
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.Nickname));
            identity.AddClaim(new Claim(ClaimTypes.Role, "user"));  // there are no admins yet

            var ticketProperties = new AuthenticationProperties(new Dictionary<string, string>() {
                    { "clientID", context.ClientId }
                });

            return new AuthenticationTicket(identity, ticketProperties);
        }
    }
}