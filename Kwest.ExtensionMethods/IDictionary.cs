﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.ExtensionMethods
{
    public static class IDictionary
    {
        public static IEnumerable<T> GetValueOrEmpty<TKey, T>(this IDictionary<TKey, IEnumerable<T>> dictionary, TKey key)
        {
            return dictionary.ContainsKey(key) ? dictionary[key] : new T[] { };
        }
    }
}
