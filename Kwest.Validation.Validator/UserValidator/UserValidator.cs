﻿using Kwest.DataAccess;
using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Register;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Validator.UserValidator
{
    public class UserValidator : BaseValidator<RegisterFormDTO>
    {
        public UserValidator(IUserRepository users) : base(users)
        {
        }

        public override IDictionary<string, List<string>> Validate(RegisterFormDTO model)
        {
            this.IsUsernameUnique(model.UserName)
                .IsNickNameUnique(model.Nickname)
                .IsEmailUnique(model.Email);

            return errors;
        }

        protected virtual UserValidator IsUsernameUnique(string username)
        {
            if (this.users.FindByUsername(username) != null)
            {
                this.AddError("UserName", "Username is already taken");
            }
            return this;
        }

        protected virtual UserValidator IsEmailUnique(string email)
        {
            if (this.users.FindByEmail(email) != null)
            {
                this.AddError("Email", "Email is already taken");
            }
            return this;
        }

        protected virtual UserValidator IsNickNameUnique(string nickname)
        {
            if(this.users.FindByNickname(nickname) != null)
            {
                this.AddError("Nickname", "Nickname is already taken");
            }
            return this;
        }
    }
}
