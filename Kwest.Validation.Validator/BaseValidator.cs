﻿using Kwest.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Validator
{
    public abstract class BaseValidator<ModelType> : IBaseValidator<ModelType>
    {
        protected IDictionary<string, List<string>> errors;
        protected IUserRepository users;

        public BaseValidator(IUserRepository users)
        {
            this.errors = new Dictionary<string, List<string>>();
            this.users = users;
        }

        public abstract IDictionary<string, List<string>> Validate(ModelType model);

        protected virtual void AddError(string key, string message)
        {
            if (this.errors.ContainsKey(key))
            {
                this.errors[key].Add(message);
            }
            else
            {
                this.errors.Add(key, new List<string>() { message });
            }
        }

        protected virtual BaseValidator<ModelType> IsUserIdValid(string userId)
        {
            if (this.users.FindById(userId) == null)
            {
                this.AddError(String.Empty, "Provided user ID doesn't exist in database");
            }
            return this;
        }
    }
}
