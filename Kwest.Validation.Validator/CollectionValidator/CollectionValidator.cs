﻿using Kwest.DataAccess;
using Kwest.DataAccess.Data.Model;
using Kwest.ViewModels.Collection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Validator.CollectionValidator
{
    public class CollectionValidator : BaseValidator<CollectionFormDTO>
    {
        private ICollectionRepository collections;

        public CollectionValidator(ICollectionRepository collections, IUserRepository users) : base(users)
        {
            this.collections = collections;
        }

        public override IDictionary<string, List<string>> Validate(CollectionFormDTO model)
        {
            this.IsTitleUnique(model.Title)
                .IsUserIdValid(model.CurrentUserId);

            return this.errors;
        }

        protected virtual CollectionValidator IsTitleUnique(string title)
        {
            if (this.collections.FindByTitle(title) != null)
            {
                this.AddError("Title", "Provided title already exists");
            }
            return this;
        }
    }
}
