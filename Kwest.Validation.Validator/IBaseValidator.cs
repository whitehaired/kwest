﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kwest.Validation.Validator
{
    public interface IBaseValidator<ModelType>
    {
        IDictionary<string, List<string>> Validate(ModelType model);
    }
}
